/* 
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#include <distributions.hpp>
#include <record.hpp>

#include <cassert>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <optional>
#include <random>
#include <string>
#include <variant>
#include <vector>

#include <effects.hpp>
#include <inference/inference.hpp>
#include <inference/metropolis/base.hpp>
#include <plate.hpp>
#include <query.hpp>

;


std::minstd_rand rng(2022);


int test_basic_node() {
    auto samp_normal_node = sample(Normal(), rng);
    std::cout << "Made a sampled node: " << samp_normal_node.string() << std::endl;

    auto obs_gamma_node = observe(Gamma(), 3.0);
    std::cout << "Made an observed node: " << obs_gamma_node.string() << std::endl;

    return 0;
}


int test_basic_record() {
    record_t<DTypes<Normal, Categorical>> r;
    auto normal_value = sample(r, "a", Normal(), rng);
    auto cat_value = sample(r, "b", Categorical(), rng);
    auto norm_obs = observe(r, "c", Normal(3.0), 5.1);

    std::cout << "Sampled normal value = " << normal_value << std::endl;
    std::cout << "Sampled cat value = " << cat_value << std::endl;
    std::cout << "Observed normal value = " << norm_obs << std::endl;

    std::cout << "Record: \n" << display(r) << std::endl;
    std::cout << "logprob(record) = " << logprob(r) << std::endl;
    std::cout << "loglikelihood(record) = " << loglikelihood(r) << std::endl;

    return 0;
}

int test_interpretation() {

    record_t<DTypes<Normal>> r;
    auto v = sample(r, "a", Normal(), rng);

    auto n = std::get<node_t<Normal>>(r.map["a"]);
    std::visit(
        overloaded(
            [&n](NodeStandard){ std::cout << "currently node a is " << n.string() << std::endl; },
            [&n](auto){ std::cout << "node is not NodeStandard, but it should be." << std::endl; }
        ),
        n.interp
    );

    // change the value and change interpretation to replayed for later execution
    std::get<node_t<Normal>>(r.map["a"]).value = 5.0;
    std::get<node_t<Normal>>(r.map["a"]).interp = NodeReplay();

    // interpretations tell us *what to do*...
    n = std::get<node_t<Normal>>(r.map["a"]);
    std::visit(
        overloaded(
            [&n](NodeReplay){ std::cout << "currently node a is " << n.string() << std::endl; },
            [&n](auto){ std::cout << "node is not NodeReplay, but it should be." << std::endl; }
        ),
        n.interp
    );

    // note same call as above
    v = sample(r, "a", Normal(), rng);
    n = std::get<node_t<Normal>>(r.map["a"]);

    // ... so after they're done, the interpretation should be what it
    // was before
    std::cout << n.string() << std::endl;
    std::visit(
        overloaded(
            [&n](NodeStandard){ std::cout << "currently node a is " << n.string() << std::endl; },
            [&n](auto){ std::cout << "node is not NodeStandard, but it should be." << std::endl; }
        ),
        n.interp
    );

    v = sample(r, "b", Normal(), rng);
    n = std::get<node_t<Normal>>(r.map["b"]);
    std::visit(
        overloaded(
            [&n](NodeStandard){ std::cout << "currently node b is " << n.string() << std::endl; },
            [&n](auto){ std::cout << "node is not NodeStandard, but it should be." << std::endl; }
        ),
        n.interp
    );

    // change the value and change interpretation to conditioned for later execution
    std::get<node_t<Normal>>(r.map["b"]).value = 5.0;
    std::get<node_t<Normal>>(r.map["b"]).interp = NodeCondition();
    // note same call as above
    v = sample(r, "b", Normal(), rng);
    n = std::get<node_t<Normal>>(r.map["b"]);
    std::visit(
        overloaded(
            [&n](NodeCondition){ std::cout << "currently node b is " << n.string() << std::endl; },
            [&n](auto){ std::cout << "node is not NodeCondition, but it should be." << std::endl; }
        ),
        n.interp
    );

    return 0;
}

double nested_normals(record_t<DTypes<Normal>>& r, std::nullopt_t) {
    auto a = sample(r, "a", Normal(), rng);
    auto b = sample(r, "b", Normal(a), rng);
    return sample(r, "c", Normal(b), rng); 
}

int test_chain_interpretation() {
    record_t<DTypes<Normal>> r;
    auto value = nested_normals(r, std::nullopt);
    std::cout << "nested normal record\n" << display(r) << std::endl;

    // now replay b with a proposed value
    std::get<node_t<Normal>>(r.map["b"]).value = 5.0;
    std::get<node_t<Normal>>(r.map["b"]).interp = NodeReplay();
    value = nested_normals(r, std::nullopt);
    std::cout << "nested normal record after b replayed\n" << display(r) << std::endl;
    
    // now condition b on a value
    std::get<node_t<Normal>>(r.map["b"]).value = -5.0;
    std::get<node_t<Normal>>(r.map["b"]).interp = NodeCondition();
    value = nested_normals(r, std::nullopt);
    std::cout << "nested normal record after b conditioned\n" << display(r) << std::endl;

    // try it with a whole new trace
    record_t<DTypes<Normal>> new_r;
    node_t<Normal> new_node;
    new_node.value = 6.5;
    new_node.interp = NodeCondition();
    new_r.addresses.push_back("b");
    new_r.map["b"] = new_node;
    value = nested_normals(new_r, std::nullopt);
    std::cout << "ab initio nested normal record with condition\n" << display(new_r) << std::endl;

    return 0;
}


int test_condition_and_replay() {
    record_t<DTypes<Normal>> r;
    pp_t<std::nullopt_t, double, Normal> f = nested_normals;
    f(r, std::nullopt);
    std::cout << "record before conditioned pass\n" << display(r) << std::endl;
    auto conditioned_f = condition<Normal>(f, "b", 3.0);
    conditioned_f(r, std::nullopt);
    std::cout << "record after conditioned pass\n" << display(r) << std::endl;
    auto really_cond_f = condition<Normal>(conditioned_f, "c", 0.0);
    really_cond_f(r, std::nullopt);
    std::cout << "record after really conditioned pass\n" << display(r) << std::endl;
    auto replayed_f = replay<Normal>(really_cond_f, "a", -1.0);
    replayed_f(r, std::nullopt);
    std::cout << "record after replay pass\n" << display(r) << std::endl;

    // do it from the beginning
    auto new_f = replay<Normal>(
        condition<Normal>(f, "a", 3.0),
        "b", 4.1
    );
    record_t<DTypes<Normal>> new_r;
    new_f(new_r, std::nullopt);
    std::cout << "new record after condition and replay\n" << display(new_r) << std::endl;

    return 0;
}

template<size_t N>
std::shared_ptr<std::array<double, N>> isotropic_normal(record_t<DTypes<static_plate<Normal, N>>>& r) {
    return sample(r, "value", static_plate<Normal, N>(2.0, 3.0), rng);
}

int test_single_type_plate() {
    constexpr size_t dim = 5;
    record_t<DTypes<static_plate<Normal, dim>>> r;
    auto value = isotropic_normal(r);
    std::cout << display(r) << std::endl;

    return 0;
}

template<size_t N>
std::shared_ptr<std::array<double, N>> plated_normal_model(
    record_t<DTypes<Normal, Gamma, static_plate<Normal, N>>>& r,
    std::shared_ptr<std::array<double, N>> data
) {
    double loc = sample(r, "loc", Normal(), rng);
    double scale = sample(r, "scale", Gamma(), rng);
    return observe(r, "obs", static_plate<Normal, N>(loc, scale), data);
}

template<size_t N>
double var_dice_model(record_t<DTypes<Normal, static_plate<Categorical, N>>>& r) {
    auto dice_values = sample(r, "dice_values", static_plate<Categorical, N>(6), rng);
    double value = 0.0;
    auto dice_values_ = *dice_values;
    for (size_t ix = 0; ix != N; ix++) {
        value += sample(r, "payoff " + std::to_string(ix), Normal(0.0, 1 + dice_values_[ix]), rng);
    }
    return value;
}

int test_multi_type_plate() {
    std::cout << "~ multi type plate with plated normal model ~" << std::endl;
    constexpr size_t dim = 5;
    record_t<DTypes<Normal, Gamma, static_plate<Normal, dim>>> r;
    std::array<double, dim> data = {1.0, 2.0, 1.0, 0.0, -5.0};
    auto d = std::make_shared<std::array<double, dim>>(data);
    auto value = plated_normal_model(r, d);
    std::cout << display(r) << std::endl;

    std::cout << "~ multi type plate with dice game ~" << std::endl;
    record_t<DTypes<Normal, static_plate<Categorical, dim>>> new_r;
    double payoff = var_dice_model(new_r);
    std::cout << display(new_r) << std::endl;
    std::cout << "You won " << payoff << " [insert currency unit here]!" << std::endl;

    return 0;
}

template<size_t N>
double shared_var_dice_model(record_t<DTypes<Normal, static_plate<Categorical, N>>>& r) {
    auto dice_values = sample(r, "dice_values", static_plate<Categorical, N>(6), rng);
    double value = 0.0;
    for (size_t ix = 0; ix != N; ix++) {
        value += sample(r, "payoff " + std::to_string(ix), Normal(0.0, 1 + dice_values->at(ix)), rng);
    }
    return value;
}

// this is an absolutely canonical model -- good reference point for future static analysis, 
// efficiency benchmarks etc

template<size_t N>
std::shared_ptr<std::array<double, N>>
shared_normal_model(
    record_t<DTypes<Normal, Gamma, static_plate<Normal, N>>>& r,
    std::shared_ptr<std::array<double, N>> data
) {
    auto loc = sample(r, "loc", Normal(), rng);
    auto inv_scale = sample(r, "inv_scale", Gamma(), rng);
    return sample(r, "obs", static_plate<Normal, N>(loc, 1.0 / inv_scale), rng, data);
}

int test_shared_multi_type_plate() {
    constexpr size_t dim = 5;
    std::cout << "~ multi type shared ptr plate with dice game ~" << std::endl;
    record_t<DTypes<Normal, static_plate<Categorical, dim>>> r;
    double payoff = shared_var_dice_model(r);
    std::cout << display(r) << std::endl;
    std::cout << "You won " << payoff << " [insert currency unit here]!" << std::endl;

    std::cout << "~ multi type shared ptr plate with canonical normal model ~" << std::endl;
    auto data = std::make_shared<std::array<double, dim>>();
    for (size_t ix = 0; ix != dim; ix++) data->operator[](ix) = ix / 2.0;
    record_t<DTypes<Normal, Gamma, static_plate<Normal, dim>>> new_r;
    auto o = shared_normal_model(new_r, data);
    std::cout << display(new_r) << std::endl;

    return 0;
}

double normal_model_with_optional(record_t<DTypes<Normal>>& r, std::unordered_map<std::string, double> data) {
    auto loc = sample(r, "loc", Normal(), rng);
    return sample(r, "obs", Normal(loc, 1.0), rng, data);
}

int test_optional_data() {
    std::cout << "~ using sample with data mapping ~" << std::endl;
    std::unordered_map<std::string, double> data_1 = {{"not_here", 12.0}};
    std::unordered_map<std::string, double> data_2 = {{"obs", 3.1}};
    record_t<DTypes<Normal>> r;
    normal_model_with_optional(r, data_1);
    std::cout << display(r) << std::endl;
    r.clear();
    normal_model_with_optional(r, data_2);
    std::cout << display(r) << std::endl;

    return 0;
}

int test_block() {
    record_t<DTypes<Normal>> r;
    pp_t<std::nullopt_t, double, Normal> f = nested_normals;
    f(r, std::nullopt);
    std::cout << "record before site is blocked\n" << display(r) << std::endl;

    auto blocked = block<Normal>(f, "b");
    r.clear();
    blocked(r, std::nullopt);
    std::cout << "record after b is blocked\n" << display(r) << std::endl;

    // condition to be equal to a value, but don't use the score
    auto block_cond = block<Normal>(condition<Normal>(f, "a", 5.0), "a");
    r.clear();
    block_cond(r, std::nullopt);
    std::cout << "record after a is conditioned then blocked\n" << display(r) << std::endl;

    return 0;
}

double my_pp(record_t<DTypes<Normal, Gamma>>& r, double data) {
    auto loc = sample(r, "loc", Normal(), rng);
    auto scale = sample(r, "scale", Gamma(), rng);
    auto obs = observe(r, "obs", Normal(loc, scale), data);
    return std::pow(obs, 2.0);
}

int test_record_level_interp() {

    record_t<DTypes<Normal, Gamma>> r;
    pp_t<double, double, Normal, Gamma> f = my_pp;
    double data = 3.0;
    f(r, data);
    std::cout << "record before block\n" << display(r) << std::endl;

    auto proposal_like = block_observe(f);
    r.clear();
    proposal_like(r, 3.0);
    std::cout << "record after block observe\n" << display(r) << std::endl; 

    auto mle_like = block_sample(f);
    r.clear();
    mle_like(r, 3.0);
    std::cout << "record after block sample\n" << display(r) << std::endl; 

    auto replaced = replace(f);
    record_t<DTypes<Normal, Gamma>> this_r;
    f(this_r, 3.0);
    std::cout << "sampled " << display(this_r) << " from program" << std::endl;
    replaced(this_r, 3.0);
    std::cout << "used same distributions instead of new ones in this sample: " << display(this_r) << std::endl;

    // building block of metropolis algos
    // use the proposal to create a record, then replay through the program
    // more to it than this (type fun + address overlap) but this is the core
    record_t<DTypes<Normal, Gamma>> new_r;
    proposal_like(new_r, data);
    std::cout << "proposal record = " << display(new_r) << std::endl;
    auto replayed = replay(f);
    auto res = replayed(new_r, data);
    std::cout << "replayed record = " << display(new_r) << std::endl;

    return 0;

}

template<size_t N>
double normal_param_model(
    record_t<DTypes<
        static_plate<Normal, N>,
        Parameter<unbounded<double>>,
        Parameter<non_negative<double>>
    >>& r,
    std::shared_ptr<std::array<double, N>> data
) {
    auto loc = param<unbounded<double>>(r, "loc")(0.0);
    auto scale = param<non_negative<double>>(r, "scale")(1.0);
    observe(r, "data", static_plate<Normal, N>(loc, scale), data);
    return loc;
}

int test_params() {
    std::cout << "~~~ testing params ~~~" << std::endl;
    constexpr int n = 3;
    record_t<DTypes<static_plate<Normal, n>, Parameter<unbounded<double>>, Parameter<non_negative<double>>>> r;
    auto data = std::make_shared<std::array<double, n>>();
    size_t ix = 0;
    for (auto x : {1.0, 2.0, -5.0}) {
        data->operator[](ix) = x;
        ++ix;
    }
    auto ret = normal_param_model<n>(r, data);
    std::cout << "program trace:" << std::endl;
    std::cout << display(r) << std::endl;

    // all-parameter models are just likelihoods
    assert(std::visit([](auto& n){ return n.logprob; }, r.map["data"]) == loglikelihood(r));

    return 0;
}


std::shared_ptr<std::array<double, 3>>
demo_slice_model(
    record_t<DTypes<
        Normal,
        static_plate<Normal, 3>,
        static_plate<Gamma, 3>,
        slice_plate<Normal, 3>
    >>& r,
    std::shared_ptr<std::array<double, 3>> data
) {
    auto latent = sample(r, "latent", Normal(), rng);
    auto means = sample(r, "means", static_plate<Normal, 3>(latent, 1.0), rng);
    auto stds = sample(r, "stds", static_plate<Gamma, 3>(2.0, 2.0), rng);
    observe(r, "data", slice_plate<Normal, 3>(means, stds), data);
    return means;
}

int test_slice_models() {
    record_t<DTypes<
        Normal,
        static_plate<Normal, 3>,
        static_plate<Gamma, 3>,
        slice_plate<Normal, 3>
    >> r;
    auto data = std::shared_ptr<std::array<double, 3>>(
        new std::array<double, 3>({1.0, 2.0, 3.0})
    );
    auto q = weighted_value<
        double,
        std::shared_ptr<std::array<double, 3>>,
        Normal,
        static_plate<Normal, 3>,
        static_plate<Gamma, 3>,
        slice_plate<Normal, 3>
    >("latent");
    
    size_t thin = 25;
    size_t burn = 1500;
    size_t nsamp = 25;
    auto opts = inf_options_t(thin, burn, burn + thin * nsamp);

    pp_t<
        std::shared_ptr<std::array<double, 3>>,
        std::shared_ptr<std::array<double, 3>>,
        Normal,
        static_plate<Normal, 3>,
        static_plate<Gamma, 3>,
        slice_plate<Normal, 3>
    > f = demo_slice_model;

    auto infer = inference<AncestorMetropolis>(f, *q, opts);
    auto res = infer(data);
    auto samples = res->sample(rng, 10);
    std::cout << "~ sampled from posterior of slice model ~" << std::endl;
    std::cout << stringify(*samples) << std::endl;

    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_basic_node(),
        test_basic_record(),
        test_interpretation(),
        test_chain_interpretation(),
        test_condition_and_replay(),
        test_block(),
        test_record_level_interp(),
        test_single_type_plate(),
        test_multi_type_plate(),
        test_shared_multi_type_plate(),
        test_params(),
        test_slice_models(),
        test_optional_data()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}
