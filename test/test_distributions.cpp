/* 
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#include <distributions.hpp>
#include <functional_util.hpp>

#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <variant>
#include <vector>
#include <random>
#include <limits>

;

bool double_allclose(double x, double y) {
    // good enough for our current purposes
    return std::fabs(x - y) <= std::numeric_limits<double>::epsilon();
}

int test_normal() {

    std::cout << "~~~ Testing Normal ~~~\n";

    // use this rng for all tests
    std::minstd_rand rng(2022);

    std::vector<Normal> normals{Normal(), Normal(3.0), Normal(3.0, 0.5)};
    auto print = [](const Normal& norm) { std::cout << norm.string() << "\n"; };

    std::for_each(normals.begin(), normals.end(), print);

    auto n = Normal();
    double val, lp;

    for (int i = 0; i != 5; i++) {
        val = n.sample(rng);
        lp = n.logprob(val);
        std::cout << "sampled " << val << " from " << n.string() << ", logprob = " << lp << "\n";
    }

    return 0;
}

int test_categorical() {

    std::cout << "~~~ Testing Categorical ~~~\n";

    std::minstd_rand rng(2022);

    std::cout << "Categorical: bernoulli distribution\n";
    auto bern = Categorical();
    int val;
    double lp;

    for (int i = 0; i != 10; i++) {
        val = bern.sample(rng);
        lp = bern.logprob(val);
        std::cout << "sampled " << val << " from " << bern.string() << ", logprob = " << lp << "\n";
    }

    Categorical uniform_cat = Categorical(5); 
    std::cout << "Created new cat: " << uniform_cat.string() << "\n";
    for (int i = 0; i != 15; i++) {
        val = uniform_cat.sample(rng);
        lp = uniform_cat.logprob(val);
        std::cout << "sampled " << val << " from " << uniform_cat.string() << ", logprob = " << lp << "\n";
    }
    double true_logprob = std::log(1.0 / 5);
    if (!double_allclose(true_logprob, lp)) {
        std::cout << "ERROR: True logprob = " << true_logprob << " but Categorical returned " << lp << "\n";
        return 1;
    }

    auto true_probs = std::vector<double>({0.1, 0.2, 0.3, 0.4});
    Categorical cat = Categorical(true_probs);
    std::cout << "Made new cat: " << cat.string() << "\n";
    int true_dim = 4;
    int cat_dim = cat.get_dim();
    if (true_dim != cat_dim) {
        std::cout << cat.string() << ": true dim = " << true_dim << " but got dim = " << cat_dim << "\n";
        return 1;
    }
    for (int i = 0; i != 10; i++) {
        val = cat.sample(rng);
        lp = cat.logprob(val);
        std::cout << "sampled " << val << " from " << cat.string() << ", logprob = " << lp << "\n";
    }

    return 0;
}

int test_gamma() {
    auto g_exp = Gamma();  // exponential distribution with rate = 1
    std::cout << "Made gamma dist: " << g_exp.string() << "\n";
    double val, lp, mean = 0.0;
    int niter = 1000;
    std::minstd_rand rng(2022);

    for (int i = 0; i != niter; i++) {
        val = g_exp.sample(rng);
        lp = g_exp.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 1.\n";  // should be about = 1

    mean = 0.0;
    auto g_exp_3 = Gamma(3.0);  // exponential distribution with rate = 3.0
    std::cout << "Made gamma dist: " << g_exp_3.string() << "\n";
    for (int i = 0; i != niter; i++) {
        val = g_exp_3.sample(rng);
        lp = g_exp_3.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 3.\n";  // should be about = 3

    mean = 0.0;
    auto g = Gamma(2.0, 3.0);  // exponential distribution with rate = 3.0
    std::cout << "Made gamma dist: " << g.string() << "\n";
    for (int i = 0; i != niter; i++) {
        val = g.sample(rng);
        lp = g.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about = 2 * 3 = 6.\n";  // should be about = 2 * 3 = 6

    return 0;
}


int test_discrete_uniform() {
    auto du = DiscreteUniform();  // exponential distribution with rate = 1
    std::cout << "Made discrete uniform dist: " << du.string() << "\n";
    double lp, mean = 0.0;
    int val;
    int niter = 1000;
    std::minstd_rand rng(20220907);

    for (int i = 0; i != niter; i++) {
        val = du.sample(rng);
        lp = du.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 0.5\n";

    du = DiscreteUniform(1, 3);
    std::cout << "Made discrete uniform dist: " << du.string() << "\n";
    mean = 0.0;

    for (int i = 0; i != niter; i++) {
        val = du.sample(rng);
        lp = du.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
            std::cout << "so prob = " << std::exp(lp) << std::endl;
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 2.0\n";


    return 0;
}


int test_uniform() {
    auto cu = Uniform();
    std::cout << "Made uniform dist: " << cu.string() << "\n";
    double val, lp, mean = 0.0;
    int niter = 1000;
    std::minstd_rand rng(20220907);

    for (int i = 0; i != niter; i++) {
        val = cu.sample(rng);
        lp = cu.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 0.5\n";

    cu = Uniform(1.0, 3.0);
    std::cout << "Made uniform dist: " << cu.string() << "\n";
    mean = 0.0;

    for (int i = 0; i != niter; i++) {
        val = cu.sample(rng);
        lp = cu.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
            std::cout << "so prob = " << std::exp(lp) << std::endl;
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 2.0\n";


    return 0;
}

int test_poisson() {
    auto pois = Poisson();
    std::cout << "Made poisson dist: " << pois.string() << "\n";
    unsigned val;
    double lp, mean = 0.0;
    int niter = 1000;
    std::minstd_rand rng(20220907);

    for (int i = 0; i != niter; i++) {
        val = pois.sample(rng);
        lp = pois.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 1.0" << std::endl;

    pois = Poisson(4.5);
    mean = 0.0;
    for (int i = 0; i != niter; i++) {
        val = pois.sample(rng);
        lp = pois.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 4.5" << std::endl;
    return 0;
}

int test_beta() {
    auto beta = Beta(1.0, 1.0);
    std::cout << "Made beta dist: " << beta.string() << "\n";
    double val;
    double lp, mean = 0.0;
    int niter = 1000;
    std::minstd_rand rng(20230319);

    for (int i = 0; i != niter; i++) {
        val = beta.sample(rng);
        lp = beta.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 0.5" << std::endl;

    beta = Beta(0.5, 0.5);
    std::cout << "Made beta dist: " << beta.string() << "\n";
    mean = 0.0;
    for (int i = 0; i != niter; i++) {
        val = beta.sample(rng);
        lp = beta.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 0.5" << std::endl;
    
    double a = 0.7, b = 3.5;
    beta = Beta(a, b);
    std::cout << "Made beta dist: " << beta.string() << "\n";
    mean = 0.0;
    for (int i = 0; i != niter; i++) {
        val = beta.sample(rng);
        lp = beta.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about " << (a / (a + b)) << std::endl;
    
    return 0;
}

int test_triangular() {

    auto t = Triangular(0, 1, 0.5);
    std::cout << "Made triangluar dist: " << t.string() << "\n";
    double val;
    double lp, mean = 0.0;
    int niter = 1000;
    std::minstd_rand rng(20230319);

    for (int i = 0; i != niter; i++) {
        val = t.sample(rng);
        lp = t.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about 0.5" << std::endl;

    t = Triangular(-1.0, 10.0, 0.0);
    std::cout << "Made triangular dist: " << t.string() << "\n";
    mean = 0.0;
    for (int i = 0; i != niter; i++) {
        val = t.sample(rng);
        lp = t.logprob(val);
        if (i < 10) {
            std::cout << "sampled " << val << " with logprob = " << lp << "\n";
        }
        mean += val;
    }
    mean /= niter;
    std::cout << "With " << niter << " trials, empirical mean: " << mean << ", should be about " << (t.a + t.b + t.c) / 3.0 << std::endl;
    
    return 0;
}

template<typename T> std::string typestring() { return "unknown"; }
template<> std::string typestring<double>() { return "double"; }
template<> std::string typestring<int>() { return "int"; }

int test_type_properties() {
    std::cout << "\n~~~ testing distribution type properties ~~~" << std::endl;
    auto n = Normal();
    std::cout << "arity of " << n.string() << " is " << arity<Normal>::value << std::endl;

    // variant constructors
    using my_var = std::variant<double, short, std::string>;
    my_var loc{3.0};
    my_var scale{1.0};
    n = Normal(loc, scale);
    std::cout << "made " << n.string() << " using variant constructor" << std::endl;

    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_normal(),
        test_categorical(),
        test_gamma(),
        test_discrete_uniform(),
        test_uniform(),
        test_poisson(),
        test_beta(),
        test_triangular(),
        test_type_properties()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}