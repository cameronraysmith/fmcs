/* 
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#include <distributions.hpp>
#include <record.hpp>

#include <inference/proposal.hpp>
#include <inference/importance/likelihood.hpp>
#include <inference/importance/generic.hpp>
#include <inference/metropolis/base.hpp>

#include <query.hpp>

#include <update.hpp>
#include <update/policies.hpp>
#include <update/update_impls.hpp>

#include <functional>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

;

std::minstd_rand rng(202);

double normal_model(record_t<DTypes<Normal, Gamma>>& r, double data) {
    auto loc = sample(r, "loc", Normal(), rng);
    auto scale = sample(r, "scale", Gamma(0.25, 5.0), rng);
    observe(r, "obs", Normal(loc, scale), data);
    return loc;
}

template<template<typename> class Policy>
void print_posterior(typed_map<Policy, double, Normal, Gamma>& m) {
    std::cout << "Loc dist is: " << m.template extract<Normal>("loc").string() << std::endl;
    std::cout << "Scale dist is: " << m.template extract<Gamma>("scale").string() << std::endl;
}

int test_update() {

    std::cout << "~~~ testing update functionality ~~~" << std::endl;

    // basic data structure for passing around distributions
    typed_map<DefaultPolicy, double, Normal, Gamma> m;
    m.insert_or_assign("obs", 5.0);
    m.insert_or_assign("loc", Normal(2.0, 2.0));
    m.insert_or_assign("scale", Gamma(2.0, 3.0));

    print_posterior(m);

    // do inference with updateable parameters
    upp_t<DefaultPolicy, double, double, Normal, Gamma> f = [](
        record_t<DTypes<Normal, Gamma>>& r, 
        typed_map<DefaultPolicy, double, Normal, Gamma>& input
    ) {
        auto loc = sample_u<Normal>(r, "loc", input, rng);
        auto scale = sample_u<Gamma>(r, "scale", input, rng);
        return observe_u<double>(r, "obs", Normal(loc, scale), input);
    };

    // draw a sample for sanity check
    record_t<DTypes<Normal, Gamma>> r;
    f(r, std::ref(m));
    std::cout << "Single record for sanity check:\n" << display(r) << std::endl;

    auto q = weighted_record<double, Normal, Gamma>();
    auto opts = inf_options_t(1000);
    auto infer = inference<LikelihoodWeighting>(f, *q, opts);

    std::cout << "doing inference with update-able model" << std::endl;
    auto res = infer(std::ref(m));

    auto updater = update<ParameterMatching, DefaultPolicy, decltype(res)>(f);
    auto new_m = updater(res);

    print_posterior(m);

    return 0;
}

template<template<typename> class Policy>
double my_model2(
    record_t<DTypes<Normal, Parameter<non_negative<double>>>>& r, 
    typed_map<Policy, double, Normal, Parameter<non_negative<double>>>& input
) {
    auto loc = sample_u<Normal>(r, "loc", input, rng);
    auto scale = param<non_negative<double>>(r, "scale")(1.0);
    return observe_u<double>(r, "obs", Normal(loc, scale), input);
}

template<template<typename> class Policy, typename... Ts>
void print_posterior(typed_map<Policy, double, Normal, Ts...>& m) {
    std::cout << "Loc dist is: " << m.template extract<Normal>("loc").string() << std::endl;
}

int test_update_filtering() {

    std::cout << "~~~ testing filtering with update, 'by hand' algo ~~~" << std::endl;

    // step 0
    using map1 = typed_map<DefaultPolicy, double, Normal, Parameter<non_negative<double>>>;

    map1 m;
    constexpr int num_iterations = 10;

    // real data
    auto noise = Normal(0.5, 0.5);
    double x = 3.0;
    
    // priors
    m.insert_or_assign("obs", x + noise.sample(rng));
    m.insert_or_assign("loc", Normal(2.0, 2.0));
    double last_obs = m.template extract<double>("obs");

    // invariants    
    upp_t<DefaultPolicy, double, double, Normal, Parameter<non_negative<double>>> f = my_model2<DefaultPolicy>;
    auto q = weighted_record<double, Normal, Parameter<non_negative<double>>>();
    auto opts = inf_options_t(1000);
    auto infer = inference<LikelihoodWeighting>(f, *q, opts);

    using res_t = decltype(std::declval<decltype(*q)>().emit());
    auto updater = update<ParameterMatching, DefaultPolicy, res_t>(f);
    
    for (int ix = 0; ix != num_iterations; ix++) {
        std::cout << "inference on iteration " << ix << std::endl;
        auto res = infer(std::ref(m));
        m = updater(res);
        /// \note very important to clear queryer cache!
        q->clear();

        // update with new observation
        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m);
        last_obs = last_obs + noise.sample(rng);
        m.insert_or_assign("obs", last_obs);
    }

    std::cout << "~~~ now, use built-in machinery ~~~" << std::endl;

    // above wrangling reduces to this
    auto algo = filter<ParameterMatching, DefaultPolicy>(f, *q, infer);

    for (int ix = 0; ix != num_iterations; ++ix) {

        // above wrangling reduces to this
        m = algo.step(m);

        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m);
        last_obs = last_obs + noise.sample(rng);
        m.insert_or_assign("obs", last_obs);
    }

    return 0;
}

template<template<typename> class Policy>
double my_model3(
    record_t<DTypes<Normal, Gamma>>& r, 
    typed_map<Policy, double, Normal, Gamma>& input
) {
    auto loc = sample_u<Normal>(r, "loc", input, rng);
    auto scale = sample_u<Gamma>(r, "scale", input, rng);
    return observe_u<double>(r, "obs", Normal(loc, scale), input);
}

int test_update_filtering_2() {
    std::cout << "~~~ comparing dist matching and normal approx ~~~" << std::endl;
    // use a distributional matching scheme and a normal approximation
    typed_map<DefaultPolicy, double, Normal, Gamma> m1;
    constexpr int num_iterations = 10;

    auto noise = Normal(0.5, 0.5);
    double x = 3.0;
    double last_obs = x = noise.sample(rng);
    
    // priors and inserted value
    m1.insert_or_assign("obs", last_obs);
    m1.insert_or_assign("loc", Normal(2.0, 2.0));
    m1.insert_or_assign("scale", Gamma(3.0));

    std::cout << "~ dist matching ~" << std::endl;
    // default policy: invariants
    upp_t<DefaultPolicy, double, double, Normal, Gamma> f1 = my_model3<DefaultPolicy>;
    auto q = weighted_record<double, Normal, Gamma>();
    auto opts = inf_options_t(1000);
    auto infer1 = inference<LikelihoodWeighting>(f1, *q, opts);

    // default policy: inference
    auto algo1 = filter<ParameterMatching, DefaultPolicy>(f1, *q, infer1);
    for (int ix = 0; ix != num_iterations; ++ix) {
        m1 = algo1.step(m1);
        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m1);
        last_obs = last_obs + noise.sample(rng);
        m1.insert_or_assign("obs", last_obs);
    }

    ///
    q->clear();

    typed_map<NormalPolicy, double, Normal, Gamma> m2;
    m2.insert_or_assign("obs", last_obs);
    // same priors, but move to a normal approximation during iteration
    m2.insert_or_assign("loc", Normal(2.0, 2.0));
    m2.insert_or_assign("scale", Gamma(3.0));

    std::cout << "~ normal approx ~" << std::endl;
    // normal policy: invariants
    upp_t<NormalPolicy, double, double, Normal, Gamma> f2 = my_model3<NormalPolicy>;
    auto infer2 = inference<LikelihoodWeighting>(f2, *q, opts);

    // normal policy: inference
    auto algo2 = filter<ParameterMatching, NormalPolicy>(f2, *q, infer2);
    for (int ix = 0; ix != num_iterations; ++ix) {
        m2 = algo2.step(m2);
        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m2);
        last_obs = last_obs + noise.sample(rng);
        m2.insert_or_assign("obs", last_obs);
    }

    return 0;
}

int test_multiple_update_types() {

    std::cout << "~~ multiple update types ~~" << std::endl;

    // set up machinery
    constexpr int num_iterations = 10;

    auto noise = Normal(0.5, 0.5);
    double x = 3.0;
    double last_obs = x + noise.sample(rng);

    typed_map<DefaultPolicy, double, Normal> m;
    m.insert_or_assign("obs", 5.0);
    m.insert_or_assign("loc", Normal(2.0, 2.0));
    m.insert_or_assign("log_scale", Normal());

    // really simple model
    upp_t<DefaultPolicy, double, double, Normal> f = [](
        record_t<DTypes<Normal>>& r, 
        typed_map<DefaultPolicy, double, Normal>& input
    ) {
        auto loc = sample_u<Normal>(r, "loc", input, rng);
        auto log_scale = sample_u<Normal>(r, "log_scale", input, rng);
        return observe_u<double>(r, "obs", Normal(loc, std::exp(log_scale)), input);
    };

    ///////////////////////////////////////////////
    // automatically derive inference algorithms //
    ///////////////////////////////////////////////

    // 1) do updates using O(1) queryer
    auto q1 = ProductGenerator<WeightedMeanStd, std::pair<double, double>, double, Normal>::generate({"loc", "log_scale"});
    auto opts = inf_options_t(1000);
    auto infer1 = inference<LikelihoodWeighting>(f, q1, opts);
    auto algo1 = filter<ParameterMatching, DefaultPolicy>(f, q1, infer1);

    std::cout << "~ using product of pairs queryer ~" << std::endl;

    print_posterior(m);
    
    for (int ix = 0; ix != num_iterations; ++ix) {
        m = algo1.step(m);
        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m);
        last_obs += noise.sample(rng);
        m.insert_or_assign("obs", last_obs);
    }

    // 2) same algo, but using O(N) queryer
    auto q2 = weighted_record<double, Normal>();
    auto infer2 = inference<LikelihoodWeighting>(f, *q2, opts);
    auto algo2 = filter<ParameterMatching, DefaultPolicy>(f, *q2, infer2);

    std::cout << "~ using full posterior queryer ~" << std::endl;

    for (int ix = 0; ix != num_iterations; ++ix) {
        m = algo2.step(m);
        std::cout << "obs = " << last_obs << std::endl;
        print_posterior(m);
        last_obs = last_obs + noise.sample(rng);
        m.insert_or_assign("obs", last_obs);
    }

    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_update(),
        test_update_filtering(),
        test_update_filtering_2(),
        test_multiple_update_types()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}
