/**
 * test_graph_basic.cpp
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.2.0
 * @date 2022-12-24
 * 
 * @copyright Copyright (c) 2022 - present. Some rights reserved..
 * 
 */

#include <cassert>
#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>

#include <distributions.hpp>
#include <query.hpp>
#include <inference/importance/likelihood.hpp>

#include <graph.hpp>
#include <graph_query.hpp>
#include <translate.hpp>

;
std::minstd_rand rng(2022);


int test_graph_basic_ops() {

    gr_pair<Normal, Gamma> gr;
    auto loc = sample_g<Normal>(gr, "loc", rng)(0.0, 1.0);
    auto loc2 = sample_g<Normal>(gr, "loc2", rng)(loc, 1.0);
    auto scale = sample_g<Gamma>(gr, "scale", rng)();
    auto obs = observe_g<Normal>(gr, "obs", 3.0)(loc2, scale);
    std::cout << display(gr) << std::endl;
    return 0;
}

graph_node<Normal> graph_normal_model(gr_pair<Normal, Gamma>& gr, double obs) {
    auto loc = sample_g<Normal>(gr, "loc", rng)(0.0, 1.0);
    auto scale = sample_g<Gamma>(gr, "scale", rng)(1.0, 0.5);
    return observe_g<Normal>(gr, "data", obs)(loc, scale);
}

double normal_model(record_t<DTypes<Normal, Gamma>>& r, double obs) {
    auto loc = sample(r, "loc", Normal(0.0, 1.0), rng);
    auto scale = sample(r, "scale", Gamma(1.0, 0.5), rng);
    return observe(r, "data", Normal(loc, scale), obs);
}

graph_node<Normal> graph_normal_model_2(gr_pair<Normal>& gr, double obs) {
    auto loc = sample_g<Normal>(gr, "loc", rng)(0.0, 1.0);
    return observe_g<Normal>(gr, "data", obs)(loc, 1.0);
}

graph_node<Normal> graph_normal_model_3(gr_pair<Normal, Gamma>& gr, double obs) {
    auto loc_scale = sample_g<Gamma>(gr, "loc_scale", rng)(1.0, 1.0);
    auto loc = sample_g<Normal>(gr, "loc", rng)(0.0, loc_scale);
    return observe_g<Normal>(gr, "data", obs)(loc, 1.0);
}

int test_basic_graph_pp() {
    std::cout << "\n~~~ test_basic_graph_pp ~~~" << std::endl;
    gr_pair<Normal, Gamma> gr;
    gpp_t<double, graph_node<Normal>, Normal, Gamma> f = graph_normal_model;
    auto obs_node = f(gr, 3.0);
    std::cout << display(gr) << std::endl;

    // note we *can* do sample based inference! ...but there will soon be better ways...
    double data = 3.0;
    auto g = to_pp(f);
    auto queryer = weighted_mean<
        gr_output<graph_node<Normal>, Normal, Gamma>, 
        Normal, Gamma
    >("loc");
    auto infer = inference<LikelihoodWeighting>(g, *queryer, inf_options_t(2000));
    auto res = infer(data);
    std::cout << "using sample based inference with graph model, E[loc | data] = " << res << std::endl;

    pp_t<double, double, Normal, Gamma> h = normal_model;
    auto queryer2 = weighted_mean<double, Normal, Gamma>("loc");
    auto infer2 = inference<LikelihoodWeighting>(h, *queryer2, inf_options_t(2000));
    res = infer2(data);
    std::cout << "using sample based inference with sample model, E[loc | data] = " << res << std::endl;

    return 0;
}

int test_graph_eq() {
    gr_pair<Normal, Gamma> gr;
    gr_pair<Normal> gr2;
    gr_pair<Normal, Gamma> gr3;
    graph_normal_model(gr, 1.0);
    graph_normal_model_2(gr2, 1.0);
    graph_normal_model_3(gr3, 1.0);

    assert(gr != gr2);
    assert(gr2 != gr3);
    assert(gr3 != gr);
    gr3.clear();
    graph_normal_model(gr3, 1.0);
    assert(gr == gr3);

    return 0;
}

unsigned
variable_mixture_model(
    gr_pair<Categorical, Gamma, Normal, Poisson>& gr, 
    std::vector<double> obs
) {
    auto num_clusters_m_1 = sample_g<Poisson>(gr, "num_clusters-1", rng)(3.0);
    std::vector<graph_node<Normal>> locs;
    std::vector<graph_node<Gamma>> scales;
    for (unsigned ix = 0; ix != num_clusters_m_1.value + 1; ix++) {
        locs.push_back( sample_g<Normal>(gr, "loc/" + stringify(ix), rng)(0.0, 3.0) );
        scales.push_back( sample_g<Gamma>(gr, "scale/" + stringify(ix), rng)(1.0, 1.0) );
    }
    for (size_t ix = 0; ix != obs.size(); ix++) {
        auto cluster = sample_g<Categorical>(gr, "cluster/" + stringify(ix), rng)(num_clusters_m_1.value + 1);
        observe_g<Normal>(gr, "obs/" + stringify(ix), obs[ix])(locs[cluster.value], scales[cluster.value]);
    }
    return num_clusters_m_1.value + 1;
}

int test_graph_query() {
    std::cout << "~~~ graph structure querying ~~~\n" << std::endl;
    // weak open universe model
    std::vector<double> cluster_data = {0.0, 0.1, -0.5, 4.2, 5.1, 3.0, -0.5, 0.67, -0.7, 6.0};
    gpp_t<
        std::vector<double>,
        unsigned,
        Categorical, Gamma, Normal, Poisson
    > graph_mm = variable_mixture_model;
    auto pp_graph_mm = to_pp(graph_mm);
    auto opts = inf_options_t(2500);

    auto map_queryer = map_structure<unsigned, Categorical, Gamma, Normal, Poisson>();
    auto map_infer = inference<LikelihoodWeighting>(pp_graph_mm, map_queryer, opts);
    auto map_res = map_infer(cluster_data);
    std::cout << "MAP structure: " << display(map_res.gr) << std::endl;

    auto mle_queryer = mle_structure<unsigned, Categorical, Gamma, Normal, Poisson>();
    auto mle_infer = inference<LikelihoodWeighting>(pp_graph_mm, mle_queryer, opts);
    auto mle_res = mle_infer(cluster_data);
    std::cout << "MLE structure: " << display(mle_res.gr) << std::endl;

    return 0;
}

graph_node<Normal> normal_with_value(gr_pair<Normal, Value<double>>& gr, double scale_value) {
    auto loc = sample_g<Normal>(gr, "loc", rng)(0.0, 1.0);
    auto scale = value_g(gr, "scale", scale_value);
    return sample_g<Normal>(gr, "data", rng)(loc, scale);
}

graph_node<Normal> normal_no_value(gr_pair<Normal, Gamma>& gr, double data_value) {
    auto loc = sample_g<Normal>(gr, "loc", rng)(0.0, 1.0);
    auto scale = sample_g<Gamma>(gr, "scale", rng)(2.0, 2.0);
    return observe_g<Normal>(gr, "data", data_value)(loc, scale);
}

int test_tracked_values_and_transforms() {
    std::cout << "~~~ test tracked values and transforms ~~~" << std::endl;
    gr_pair<Normal, Value<double>> gr;
    auto ret = normal_with_value(gr, 3.0);
    std::cout << display(gr) << std::endl;

    return 0;
}

int test_elementary_translation() {
    std::string program_name = "my_cool_program";
    auto bind = [&program_name](auto& n) -> std::string { 
        return n.obs ? observe_statement(n, program_name) : sample_statement(n, program_name);
    };

    gr_pair<Categorical> cat_gr;
    sample_g<Categorical>(cat_gr, "test_cat", rng)(5);
    std::string trans = std::visit(bind, cat_gr.g.map["test_cat"]);
    std::cout << trans << std::endl;

    std::cout << "~ multiple sites, sampled and observed ~" << std::endl;
    gr_pair<Gamma, Normal, Value<double>, Categorical> hetero_gr;
    auto hyperloc = value_g(hetero_gr, "hyperloc", 0.0);
    auto hyperscale = value_g(hetero_gr, "hyperscale", 2.0);
    auto loc = sample_g<Normal>(hetero_gr, "loc", rng)(hyperloc, hyperscale);
    auto scale = sample_g<Gamma>(hetero_gr, "scale", rng)(2.0, 2.0);
    observe_g<Normal>(hetero_gr, "data", 3.0)(loc, scale);
    auto cat_sample = sample_g<Categorical>(hetero_gr, "cat_sample", rng)(4);
    std::vector<double> probs = {0.1, 0.2, 0.5, 0.2};
    observe_g<Categorical>(hetero_gr, "cat_obs", cat_sample.value)(probs);

    std::cout << std::visit(bind, hetero_gr.g.map["hyperscale"]) << std::endl;
    std::cout << std::visit(bind, hetero_gr.g.map["hyperloc"]) << std::endl;
    std::cout << std::visit(bind, hetero_gr.g.map["loc"]) << std::endl;
    std::cout << std::visit(bind, hetero_gr.g.map["scale"]) << std::endl;
    std::cout << std::visit(bind, hetero_gr.g.map["data"]) << std::endl;
    std::cout << std::visit(bind, hetero_gr.g.map["cat_sample"]) << std::endl;
    std::cout << std::visit(bind, hetero_gr.g.map["cat_obs"]) << std::endl;

    return 0;
}

int test_translation_integration() {

    gr_pair<Normal, Gamma> gr;
    auto ret = normal_no_value(gr, 3.0);
    program_info info = { .program_name = "my_program", .inference_algo = glppl_algos::likelihood_weighting{} };
    auto rep = construct_program(gr, info, "out/");

    std::cout << "\n\n~ translation h file ~\n" << std::endl;
    std::cout << rep.h_file << std::endl;
    std::cout << "\n\n~ translation c file ~\n" << std::endl;
    std::cout << rep.c_file << std::endl;

    return 0;
}

double normal_no_value_lppl(record_t<DTypes<Normal, Gamma>>& r, double data_value) {
    auto loc = sample(r, "loc", Normal(0.0, 1.0), rng);
    auto scale = sample(r, "scale", Gamma(2.0, 2.0), rng);
    return observe(r, "data", Normal(loc, scale), data_value);
}

int test_translation_timing() {

    // to be timed in C -- we will use same number of inference iterations, etc.
    gr_pair<Normal, Gamma> gr;
    auto ret = normal_no_value(gr, 3.0);
    program_info info = { .program_name = "timing_test_program", .inference_algo = glppl_algos::likelihood_weighting{} };
    auto rep = construct_program(gr, info, "timing_test/");

    // instead of an equivalent in glppl we will use fmcs which is lighter weight and hence should be faster for sample-based
    // inference where we are not trying to infer posterior structure
    pp_t<double, double, Normal, Gamma> f = normal_no_value_lppl;
    auto opts = inf_options_t(100);
    auto queryer = weighted_record<double, Normal, Gamma>();
    auto infer = inference<LikelihoodWeighting>(f, *queryer, opts);
    double data = 3.0;

    std::vector<double> times;
    int num_times = 100;

    for (size_t ix = 0; ix != num_times; ix++) {
        auto start_time = std::chrono::steady_clock::now();
        auto result = infer(data);
        auto elapsed = std::chrono::steady_clock::now() - start_time;
        auto tt = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
        times.push_back(tt);
        queryer->clear();
    }

    std::ofstream out_file;
    out_file.open("./timing_test/out/timing_test_program_results_cpp.csv");
    out_file << "time\n";
    for (size_t ix = 0; ix != num_times; ix++) {
        out_file << times[ix] << "\n";
    }
    out_file.close();

    return 0;
}

int main(int argc, char ** argv) {
    auto status = std::vector<int>({
        test_graph_basic_ops(),
        test_basic_graph_pp(),
        test_graph_eq(),
        test_graph_query(),
        test_tracked_values_and_transforms(),
        test_elementary_translation(),
        test_translation_integration(),
        test_translation_timing()
    });
    if (*std::max_element(status.begin(), status.end()) > 0) {
        std::cout << "~~~ Test failed; read log for more. ~~~\n";
        return 1;
    } else {
        std::cout << "~~~ All tests passed. ~~~\n";
        return 0;
    }
}
