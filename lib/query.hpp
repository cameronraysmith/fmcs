/**
 * \file
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#ifndef QUERY_H
#define QUERY_H

#include <cstddef>
#include <initializer_list>
#include <limits>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include <distribution_traits.hpp>
#include <record.hpp>

/**
 * @brief Options used by all inference algorithms.
 * 
 */
struct inf_options_t {
    size_t _thin;
    size_t _burn;
    size_t _num_iterations;
    size_t _num_complete;
    unsigned _seed;

    inf_options_t(size_t thin, size_t burn, size_t num_iterations, unsigned seed=2022)
        : _num_iterations(num_iterations), _num_complete(0), _thin(thin), _burn(burn), _seed(seed) {}

    inf_options_t(size_t num_iterations, unsigned seed=2022)
        : _num_iterations(num_iterations), _num_complete(0), _thin(0), _burn(0), _seed(seed) {}

    std::string string() {
        std::string s = "inf_options_t{ thin = ";
        s += stringify(_thin) + ", burn = " + std::to_string(_burn) + ", num_iterations = ";
        s += std::to_string(_num_iterations) + ", num_complete = " + std::to_string(_num_complete);
        s += ", seed = " + std::to_string(_seed) + " }";
        return s;
    }
};

/**
 * @brief Interface to all querying mechanisms for sample-based inference (and possibly other
 *  inference algorithms). Much computation that is associated with inference is implemented
 *  by subclasses of Queryer (rather than in inference algorithms themselves, as is often done).
 * 
 * @tparam V The type that will be returned when the emit method is called
 * @tparam O The type returned by the probabilistic program that will be queried
 * @tparam Ts The types of distributions in the probabilistic program that will be queried
 */
template<
    template<typename, typename, typename...> class Impl,
    typename V, 
    typename O, 
    typename... Ts
>
struct Queryer {
    /**
     * @brief Updates the state of the queryer. 
     * 
     * @param r a record with state set by the probabilistic program being queried
     * @param output the output of the probabilistic program being queried
     * @param weight the weight associated with the query. The weight will be interpreted
     *  as the posterior log probability of the record.
     * @param opts inference options passed to the inference algorithm
     */
    void update(record_t<DTypes<Ts...>>& r, O output, double weight, inf_options_t opts) {
        return static_cast<Impl<V, O, Ts...>*>(this)->update(r, output, weight, opts);
    }
    
    /**
     * @brief Emits the result of the query.
     * 
     */
    V emit() {
        return static_cast<Impl<V, O, Ts...>*>(this)->emit();
    }
};

/**
 * @brief streaming logsumexp computation, see https://arxiv.org/abs/1805.02867
 * 
 * 
 * @param _alpha running maximum for normalization
 * @param _r the sum term while controlling for the maximum
 * @param weight the most recent (log) weight
 * @return double 
 */
double logsumexp_streaming(double& _alpha, double& _r, const double& weight) {
    if (weight <= _alpha) {
        _r += std::exp(weight + _alpha);
    } else {
        _r *= std::exp(_alpha - weight);
        _r += 1.0;
        _alpha = weight;
    }
    return std::log(_r) + _alpha;
};

/**
 * @brief Computes a streaming log-sum-exp.
 * 
 * @tparam V 
 * @tparam O 
 * @tparam Ts 
 */
template<typename, typename O, typename... Ts>
struct LogSumExpQ : Queryer<LogSumExpQ, double, O, Ts...> {

    double _lse_weights;
    double _alpha;
    double _r;

    LogSumExpQ() : _lse_weights(0.0), _alpha(-std::numeric_limits<double>::infinity()), _r(0.0) {}

    friend double logsumexp_streaming(double& _alpha, double& _r, const double& weight);

    void update(record_t<DTypes<Ts...>>& r, O output, double weight, inf_options_t opts) {
        _lse_weights = logsumexp_streaming(_alpha, _r, weight);
    }

    void clear() {
        _lse_weights = 0.0;
        _alpha = -std::numeric_limits<double>::infinity();
        _r = 0.0;
    }

    double emit() { return _lse_weights; }

};

template<typename O, typename... Ts>
auto log_sum_exp_q() {
    return std::make_shared<LogSumExpQ<double, O, Ts...>>();
}

/**
 * @brief Access to all sample weights.
 * 
 * @tparam V Type returned by the queryer
 * @tparam O Output type of the probabilistic program over which to do inference
 * @tparam Ts Types in the program
 */
template<typename V, typename O, typename... Ts>
struct Weighted
    : Queryer<Weighted, V, O, Ts...>,
      LogSumExpQ<double, O, Ts...> {

    std::vector<double> w;

    Weighted() : LogSumExpQ<double, O, Ts...>() {}

    /**
     * @brief Computes \f$\log Z = \log \sum_{1 \leq n \leq N} \exp w_n \f$.
     * 
     * The interpretation of this number depends on the inference algorithm used with the queryer.
     * If an importance sampling algorithm is used, this is the evidence. This number should always
     * be equal to 1 when using a MCMC algorithm.
     * 
     * @param r the record to use for computing the weight
     * @param output the output of the program (unused, for API compliance)
     * @param weight the log weight associated with the record
     * @param opts the inference options
     */
    void update(record_t<DTypes<Ts...>>& r, O output, double weight, inf_options_t opts) {
        LogSumExpQ<double, O, Ts...>::update(r, output, weight, opts);
        w.push_back(weight);
    }

    /**
     * @brief Resets all values associated with the queryer.
     * 
     */
    void clear() {
        LogSumExpQ<double, O, Ts...>::clear();
        w.clear();
    }
    /**
     * @brief Returns \f$\log Z = \log \sum_{1 \leq n \leq N} \exp w_n \f$.
     * 
     * The interpretation of this number depends on the inference algorithm used with the queryer.
     * If an importance sampling algorithm is used, this is the evidence. This number should always
     * be equal to 1 when using a MCMC algorithm.
     * 
     * @return double 
     */
    double lse_weights() { return this->_lse_weights; }
};

/**
 * @brief Optimizes a value function and returns the argmax value.
 * 
 * Given a stream of records \f$r\f$, outputs \f$o\f$, and computed log weights \f$w\f$
 * from a posterior distribution \f$p(z|x)\f$, a mapping \f$ v = m(r, o, w) \f$ from into the value
 * type \f$v\f$, and a value function \f$V(r, o, w)\f$ into the reals, computes the 
 * (approximate global) maximum of \f$V\f$ and returns \f$v^* = \arg \max_{v = m(r, o, w)} V(r, o, w)\f$.
 * 
 * @tparam V The type returned by the mapping function
 * @tparam O The output type of the probabilistic program
 * @tparam Ts The types of distributions in the probabilistic program
 */
template<typename V, typename O, typename... Ts>
struct Optimizer : Queryer<Optimizer, V, O, Ts...> {

    using map_fn_t = std::function<V(record_t<DTypes<Ts...>>&, O&, double& w)>;
    using value_fn_t = std::function<double(record_t<DTypes<Ts...>>&, O&, double& w)>;

    V opt;
    double value;
    map_fn_t map_fn;
    value_fn_t value_fn;

    /**
     * @brief Construct a new Optimizer object
     * 
     * @param map_fn Extracts or creates the value from the record, output, and weight
     * @param value_fn Assigns a value to the combination of record, output, and weight
     */
    Optimizer(
        map_fn_t map_fn,
        value_fn_t value_fn
    ) : value(-std::numeric_limits<double>::infinity()), map_fn(map_fn), value_fn(value_fn) {}

    void update(record_t<DTypes<Ts...>>& r, O output, double weight, inf_options_t opts) {
        double this_value = value_fn(r, output, weight);
        if (this_value > value) {
            value = this_value;
            opt = map_fn(r, output, weight);
        }
    }

    void clear() {
        value = -std::numeric_limits<double>::infinity();
    }

    V emit() { return opt; }

    /**
     * @brief Returns the (approximate global) maximum of the value function
     * 
     * @return double 
     */
    double optimum_value() { return value; }

};

template<typename V, typename O, template<class, class, class...> class Q, typename... Ts>
using queryer_map_t = std::unordered_map<std::string, std::shared_ptr<Q<V, O, Ts...>>>;

// TODO [priority:MED]
// HPDS -- should return the highest posterior density set composed of copies of records
// HPDI -- should return the highest posterior density interval [l, h] for an address
// Relational -- should return copies of values or records where a relation is satisfied
//   along with their (long) sample weights. Should expose a method fo random sampling from
//   the collection.
// Probability -- estimate probability of event from discrete random variable 

struct collection_t { 
    // \todo is this struct necessary? 
    Categorical _c;
    collection_t(Categorical c) : _c(std::move(c)) {}
};

/**
 * @brief A collection of sampled values.
 * 
 * @tparam V the type of the sampled value
 */
template<typename V>
struct value_collection_t : collection_t {
    std::vector<V> _v;

    size_t size() { return _v.size(); }

    value_collection_t(value_collection_t<V>&& other) {
        _v = other._v;
        this->_c = other._c;

        other._v = std::vector<V>();
        other._c = Categorical();
    }

    value_collection_t(std::vector<V> v, Categorical c)
        : _v(std::move(v)), collection_t(c) {}

    /**
     * @brief Samples single value from the empirical posterior
     * 
     * @tparam RNG the type of the PRNG used to perform sampling
     * @param rng the PRNG used to perform sampling
     * @return std::shared_ptr<V> 
     */
    template<typename RNG>
    std::shared_ptr<V> sample(RNG& rng) {
        return std::make_shared<V>(_v[this->_c.sample(rng)]);
    }

    /**
     * @brief Returns the empirical probability of the sample at the specified index
     * 
     * @param ix The index of the sample. *Does not bounds check!*
     * @return double The probability of the sample at the index
     */
    double prob_at(size_t ix) { return this->_c.at(ix); }

    /**
     * @brief Samples n values with replacement from the empirical posterior
     * 
     * @tparam RNG 
     * @param rng 
     * @param n 
     * @return std::shared_ptr<std::vector<V>> 
     */
    template<typename RNG>
    std::shared_ptr<std::vector<V>> sample(RNG& rng, size_t n) {
        auto out = std::make_shared<std::vector<V>>();
        for (size_t ix = 0; ix != n; ix++) out->push_back(_v[this->_c.sample(rng)]);
        return out;
    }

    /**
     * @brief Map the vector of posterior samples thorugh a callable.
     * 
     * map :: (v -> v) -> [v] -> [v]
     * 
     * \todo efficiency!
     * 
     * @tparam Callable 
     * @param f 
     * @return value_collection_t<V>& 
     */
    template<typename Callable>
    value_collection_t<V>& map(Callable f) {
        for (size_t ix = 0; ix != _v.size(); ix++) {
            _v[ix] = f(_v[ix]);
        }
        return *this;
    }

    /**
     * @brief Filter the vector of posterior samples through a callable.
     * 
     * filter :: (v -> bool) -> [v] -> [v]
     * 
     * \todo efficiency!
     * 
     * @tparam Callable 
     * @param f 
     * @return value_collection_t<V>& 
     */
    template<typename Callable>
    value_collection_t<V>& filter(Callable f) {
        std::vector<V> new_v;
        for (size_t ix = 0; ix != _v.size(); ix++) {
            if (f(_v[ix])) new_v.push_back(_v[ix]);
        }
        _v = new_v;
        return *this;
    }

    /**
     * @brief Reduce the vector of posterior results to a scalar using a callable.
     * 
     * reduce :: ([v] -> v) -> [v] -> v
     * 
     * \todo efficiency!
     * \todo implement foldl' and keep this as a special case
     * 
     * @tparam Callable 
     * @param fn 
     * @return V 
     */
    template<typename Callable>
    V reduce(Callable fn) { return fn(_v); }

};

/**
 * @brief A collection of weighted values from a single site.
 * 
 * This is the empirical marginal posterior distribution at the given address.
 *  The memory use of this object scales as O(num_samples).
 * 
 * @tparam V the type of the returned value
 * @tparam O the output type of the probabilistic program to be queried
 * @tparam Ts the types in the probabilistic program
 */
template<typename V, typename O, typename... Ts>
class WeightedValue;

template<typename V, typename O, typename... Ts>
class WeightedValue<std::unique_ptr<value_collection_t<V>>, O, Ts...> 
    : public Queryer<WeightedValue, std::unique_ptr<value_collection_t<V>>, O, Ts...>,
      public Weighted<std::unique_ptr<value_collection_t<V>>, O, Ts...> {

    protected:
        std::string _address;
        std::vector<V> v;

    public:
        WeightedValue(std::string address) : _address(address) {}
        void update(record_t<DTypes<Ts...>>& r, O output, double weight, inf_options_t opts) {
            auto get_value = [](auto node) -> unique_variant_t<var_ret_t<Ts...>> { return node.value; };
            if (r.map.contains(_address)) {
                auto value = std::visit(get_value, r.map[_address]);
                if (std::holds_alternative<V>(value)) {
                    Weighted<std::unique_ptr<value_collection_t<V>>, O, Ts...>::update(r, output, weight, opts);
                    v.push_back(std::get<V>(value));
                }
            }
        }
        std::unique_ptr<value_collection_t<V>> emit() {
            // w_n = w'_n / sum w'_n = exp(log w'_n - log sum w'_n) = exp( weight_n - logsumexp weight_n)
            for (size_t ix = 0; ix != this->w.size(); ix++) this->w[ix] = std::exp(this->w[ix] - this->_lse_weights);
            auto c = Categorical(std::move(this->w));
            return std::make_unique<value_collection_t<V>>(v, c);
        }

        /**
         * @brief Resets the instance
         * 
         */
        void clear() {
            Weighted<std::unique_ptr<value_collection_t<V>>, O, Ts...>::clear();
            v.clear();
        }
};

/**
 * @brief Factory function for WeightedValue
 * 
 */
template<typename V, typename O, typename... Ts>
auto weighted_value(std::string address) {
    return std::make_unique<WeightedValue<std::unique_ptr<value_collection_t<V>>, O, Ts...>>(address);
}

/**
 * @brief Collects records generated by an inference algorithm into an empirical posterior
 *  distribution over records and output values
 * 
 * @tparam O the output type of the probabilistic program to be queried
 * @tparam Ts the types in the probabilistic program
 */
template<typename O, typename... Ts>
struct record_collection_t : value_collection_t<record_t<DTypes<Ts...>>> {
    std::vector<O> _o;

    record_collection_t(record_collection_t<O, Ts...>&& other) {
        this->_v = other._v;
        _o = other._o;
        this->_c = other._c;

        other._v = std::vector<record_t<DTypes<Ts...>>>();
        other._o = std::vector<O>();
        other._c = Categorical();
    }

    std::unordered_set<std::string> all_addresses() {
        std::unordered_set<std::string> res;
        for (auto& r : this->_v) {
            for (auto& address : r.addresses) {
                if (!res.contains(address)) res.insert(address);
            }
        }
        return res;
    }

    template<typename V>
    value_collection_t<V> at(std::string address) {
        std::vector<V> v;

        for (auto& r : this->_v) {
            v.push_back(
                std::get<V>(
                    std::visit(
                        [](auto& n) -> unique_variant_t<var_ret_t<Ts...>> { return n.value; },
                        r.map[address]
                    )
                )
            );
        }
        return value_collection_t<V>(v, this->_c);
    }

    record_collection_t(std::vector<record_t<DTypes<Ts...>>> v, std::vector<O> o, Categorical c) 
        : value_collection_t<record_t<DTypes<Ts...>>>(v, c), _o(o) {}

    /**
     * @brief Samples a single output value of the probabilistic program from the empirical posterior.
     * 
     * @tparam RNG the type of the PRNG used to sample
     * @param rng the PRNG used to sample
     * @return std::shared_ptr<O> 
     */
    template<typename RNG>
    std::shared_ptr<O> sample_output(RNG& rng) {
        return std::make_shared<O>(_o[this->_c.sample(rng)]);
    }

    /**
     * @brief Samples n output values of the probabilistic program from the empirical posterior
     * 
     * @tparam RNG 
     * @param rng 
     * @param n 
     * @return std::shared_ptr<std::vector<O>> 
     */
    template<typename RNG>
    std::shared_ptr<std::vector<O>> sample_output(RNG& rng, size_t n) {
        auto out = std::make_shared<std::vector<O>>();
        for (size_t ix = 0; ix != n; ix++) out->push_back(_o[this->_c.sample(rng)]);
        return out;
    }

};

/**
 * @brief A collection of weighted records. 
 * 
 * This is the empirical joint posterior distribution. The memory use of this 
 *  object scales as O(num_samples).
 * 
 */
template<typename, typename O, typename... Ts>
class WeightedRecord 
    : public Queryer<WeightedRecord, std::shared_ptr<record_collection_t<O, Ts...>>, O, Ts...>,
      public Weighted<std::shared_ptr<record_collection_t<O, Ts...>>, O, Ts...> {

    protected:
        std::vector<record_t<DTypes<Ts...>>> v;
        std::vector<O> o;

    public:
        void update(record_t<DTypes<Ts...>>& r, O output, double weight, inf_options_t opts) {
            Weighted<std::shared_ptr<record_collection_t<O, Ts...>>, O, Ts...>::update(r, output, weight, opts);
            // aware of copy overhead, unavoidable since need to access all records after inference
            v.push_back(r);
            o.push_back(output);
        }
        std::shared_ptr<record_collection_t<O, Ts...>> emit() {
            // w_n = w'_n / sum w'_n = exp(log w'_n - log sum w'_n) = exp( weight_n - logsumexp weight_n)
            for (size_t ix = 0; ix != this->w.size(); ix++) this->w[ix] = std::exp(this->w[ix] - this->_lse_weights);
            auto c = Categorical(std::move(this->w));
            return std::make_shared<record_collection_t<O, Ts...>>(v, o, c);
        }

        /**
         * @brief Resets the instance
         * 
         */
        void clear() {
            Weighted<std::shared_ptr<record_collection_t<O, Ts...>>, O, Ts...>::clear();
            v.clear();
            o.clear();
        }
};

/**
 * @brief Factory function for WeightedRecord
 * 
 */
template<typename O, typename... Ts>
auto weighted_record() {
    return std::make_unique<WeightedRecord<std::shared_ptr<record_collection_t<O, Ts...>>, O, Ts...>>();
}


/**
 * @brief Computes the mean of the specified sample site with O(1) memory.
 * 
 */
template<typename, typename O, typename... Ts>
class WeightedMean 
    : public Queryer<WeightedMean, double, O, Ts...>,
      public LogSumExpQ<double, O, Ts...> {

    protected:
        std::string _address;
        double _mean;
        
    public:
        WeightedMean(std::string address) : _address(address), _mean(0.0) {}
        void update(record_t<DTypes<Ts...>>& r, O output, double weight, inf_options_t opts) {
            auto get_value = [](auto node) -> unique_variant_t<var_ret_t<Ts...>> { return node.value; };
            if (r.map.contains(_address)) {
                auto value = std::visit(get_value, r.map[_address]);
                if (std::holds_alternative<double>(value)) {
                    LogSumExpQ<double, O, Ts...>::update(r, output, weight, opts);
                    _mean += std::exp(weight - this->_lse_weights) * (std::get<double>(value) - _mean);
                }
            }
        }

        double emit() {
            return _mean;
        }

        void clear() {
            LogSumExpQ<double, O, Ts...>::clear();
        }
};

/**
 * @brief factory function for WeightedMean
 * 
 */
template<typename O, typename... Ts>
auto weighted_mean(std::string address) {
    return std::make_unique<WeightedMean<double, O, Ts...>>(address);
}

/**
 * @brief Computes the mean and standard deviation of the specified sample site with O(1) memory.
 * 
 * @tparam O The output type of the probabilistic program
 * @tparam Ts The types of distributions contained in the probabilistic program
 */
template<typename, typename O, typename... Ts>
struct WeightedMeanStd
    : public Queryer<WeightedMeanStd, std::pair<double, double>, O, Ts...> {

    std::string address_;
    std::shared_ptr<LogSumExpQ<double, O, Ts...>> w;
    double mean_old;
    double mean_;
    double std_;
        
    WeightedMeanStd(std::string address) 
        : address_(address), w(log_sum_exp_q<O, Ts...>()), mean_old(0.0), mean_(0.0), std_(0.0) {}

    void update(record_t<DTypes<Ts...>>& r, O output, double weight, inf_options_t opts) {
        auto get_value = [](auto node) -> unique_variant_t<var_ret_t<Ts...>> { return node.value; };
        if (r.map.contains(address_)) {
            auto value = std::visit(get_value, r.map[address_]);
            if (std::holds_alternative<double>(value)) {
                // update weighting scheme
                w->update(r, output, weight, opts);
                // compute the streaming weighted mean
                mean_old = mean_;
                double v = std::get<double>(value);
                mean_ = mean_old + std::exp(weight - w->emit()) * (v - mean_old);
                // compute the streaming weighted std
                std_ += std::exp(weight) * (v - mean_) * (v - mean_old);
            }
        }
    }

    std::pair<double, double> emit() {
        double std = std::sqrt(std::exp(std::log(std_) - w->emit()));
        return std::make_pair(mean_, std);
    }

    void clear() {
        w->clear();
    }

};

template<typename O, typename... Ts>
auto weighted_mean_std(std::string address) {
    return std::make_unique<WeightedMeanStd<std::pair<double, double>, O, Ts...>>(address);
}

/**
 * @brief Generates a queryer that returns a fully factored collection of views.
 * 
 * Suppose that \f$V[p(z_a|x)]\f$ is a view of the posterior at address \f$a\f$. 
 * Then `Productgenerator` generates the view \f$V[p(z|x)] = \prod_a V[p(z_a|x)]\f$.
 * 
 * @tparam Impl The single site view. Must have a constructor taking a single `std::string` argument
 *  the address.
 * @tparam V the type that `Impl` returns via `.emit()`
 * @tparam O the type returned by the probabilistic program
 * @tparam Ts the distribution types in the probabilistic program.
 */
template<
    template<typename, typename, typename...> class Impl,
    typename V, 
    typename O, 
    typename... Ts
>
struct ProductGenerator {

    using VContainer = std::unordered_map<std::string, V>;
    using DContainer = std::unordered_map<std::string, std::variant<Ts...>>;

    /**
     * @brief Product of values (mapping from address to result type of underlying queryer)
     *  and distributions (mapping from address to first-encountered distribution at that address)
     * 
     */
    struct EmitType {
        VContainer values;
        DContainer distributions;
    };

    /**
     * @brief A queryer that emits \f$V[p(z|x)] = \prod_a V[p(z_a|x)]\f$.
     * 
     * @tparam O_ The output type of the probabilistic program
     * @tparam Ts_ The types of distributions in the probabilistic program
     */
    template<typename, typename, typename...>
    struct Q : Queryer<Q, EmitType, O, Ts...>  {

        using QContainer = std::unordered_map<std::string, Impl<V, O, Ts...>>;

        QContainer qs;
        DContainer ds;
        
        Q(std::vector<std::string>& addrs) {
            for (const auto& addr : addrs) {
                qs.insert({addr, Impl<V, O, Ts...>(addr)});
            }
        }

        void update(record_t<DTypes<Ts...>>& r, O output, double weight, inf_options_t opts) {
            for (auto& [_, q] : qs) q.update(r, output, weight, opts);
            for (const auto& [address, node] : r.map) {
                if (!ds.contains(address)) {
                    ds.insert_or_assign(
                        address, 
                        std::visit(
                            [](auto&& node) -> std::variant<Ts...> { return node.distribution; },
                            r.map[address]
                        )
                    );
                }                
            }
        }

        /**
         * @brief Returns a product of maps from addresses to inferred values
         *  and maps from addresses to distributions at those sites
         * 
         * @return EmitType 
         */
        EmitType
        emit() {
            EmitType res;
            for (auto& [k, q] : qs) {
                res.values.insert_or_assign(k, q.emit());
            }
            res.distributions = ds;
            return res;
        }

        void clear() {
            for (auto& [_, q] : qs) q.clear();
        }

    };

    /**
     * @brief Generates the queryer that emits \f$V[p(z|x)] = \prod_a V[p(z_a|x)]\f$, where
     *  \f$a \in \f$ `addresses`.
     * 
     * @param addresses The addresses to query
     * @return Q<EmitType, O, Ts...> 
     */
    static
    Q<EmitType, O, Ts...>
    generate(std::vector<std::string> addresses) {
        return Q<EmitType, O, Ts...>(addresses);
    }

};


#endif  // QUERY_H
