/**
 * @file default_translations.hpp
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.2.0
 * @date 2023-01-18
 * 
 * @copyright Copyright (c) 2023 - present. Some rights reserved..
 * 
 */

#ifndef GLPPL_DEFAULT_TRANSLATIONS_H
#define GLPPL_DEFAULT_TRANSLATIONS_H

#include <cassert>
#include <string>

#include <distributions.hpp>
#include <distributions/graph_distributions.hpp>

#define GLPPL_NODE_TYPE_NULL "NODE_TYPE_NULL"

;

/**
 * @brief 
 * 
 */
struct node_spec {
    std::string dist_type;
    std::string dist_params;
    std::string node_name;
    std::string dist_sample_type;
};

/**
 * @brief 
 * 
 * @tparam D 
 */
template<class D>
struct translation { 
    constexpr static bool value = false;
    node_spec operator()(graph_node<D>& gn) {
        return (node_spec) {"NULL", "NULL", "NULL", "NULL"};
    }
};

template<typename D>
node_spec translate_node(graph_node<D>& gn) {
    return translation<D>()(gn);
}

template<typename D>
std::string node_type() { return GLPPL_NODE_TYPE_NULL; }

/**
 * @brief 
 * 
 * @tparam D 
 */
template<class D>
struct auxiliary_info {
    std::string operator()(graph_node<D>& gn) { return ""; }
};

bool all_param(std::vector<std::string>& v) {
    for (auto& element : v) {
        if (element != GLPPL_OPT_NULL) return false;
    }
    return true;
}

bool any_param(std::vector<std::string>& v) {
    for (auto& element : v) {
        if (element == GLPPL_OPT_NULL) return true;
    }
    return false;
}

template<class D>
std::string parent_params(graph_node<D>& gn) {
    if (any_param(gn.parents)) {
        assert(false && "for translation, specify all parents as graph_node (potentially using Value<V>) or none");
    }
    std::string params;
    for (size_t ix = 0; ix != gn.parents.size() - 1; ix++) {
        params += stringify(gn.parents[ix]) + ", ";
    }
    params += stringify(gn.parents[gn.parents.size() - 1]);
    return params;
}

template<>
struct translation<Normal> {
    constexpr static bool value = true;
    node_spec operator()(graph_node<Normal>& gn) {
        std::string params;
        if ((gn.parents.size() < 1) | all_param(gn.parents)) {
            params = std::to_string(gn.distribution.loc) + ", " + std::to_string(gn.distribution.scale);
        } else {
            params = parent_params(gn);
        }
        return (node_spec) {
            "Normal",
            params,
            gn.address,
            "double"
        };
    }
};

template<>
std::string node_type<Normal>() { return "struct Normal"; }

template<>
struct translation<Gamma> {
    constexpr static bool value = true;
    node_spec operator()(graph_node<Gamma>& gn) {
        std::string params = std::to_string(gn.distribution.k) + ", " + std::to_string(gn.distribution.theta);
        params += ", (struct Normal) {0.0, 1.0}";  // ccyan gamma distributions use normal variates internally
        return (node_spec) {
            "Gamma",
            params,
            gn.address,
            "double"
        };
    }
};

template<>
std::string node_type<Gamma>() { return "struct Gamma"; }

template<>
struct translation<Value<double>> {
    constexpr static bool value = true;
    node_spec operator()(graph_node<Value<double>>& gn) {
        return (node_spec) {
            "",
            std::to_string(gn.value),
            gn.address,
            "double"
        };
    }
};

template<>
struct translation<Value<unsigned>> {
    constexpr static bool value = true;
    node_spec operator()(graph_node<Value<unsigned>>& gn) {
        return (node_spec) {
            "",
            std::to_string(gn.value),
            gn.address,
            "unsigned"
        };
    }
};

template<>
struct translation<Value<unsigned long>> {
    constexpr static bool value = true;
    node_spec operator()(graph_node<Value<unsigned long>>& gn) {
        return (node_spec) {
            "",
            std::to_string(gn.value),
            gn.address,
            "unsigned long"
        };
    }
};

template<>
struct translation<Value<int>> {
    constexpr static bool value = true;
    node_spec operator()(graph_node<Value<int>>& gn) {
        return (node_spec) {
            "",
            std::to_string(gn.value),
            gn.address,
            "int"
        };
    }
};

template<>
struct auxiliary_info<Categorical> {
    std::string operator()(graph_node<Categorical>& gn) {
        std::string probs, cum_probs;
        double cum_val = 0.0;
        std::vector<double> prob_vals = gn.distribution.get_probs();
        for (size_t ix = 0; ix != prob_vals.size() - 1; ix++) {
            probs += std::to_string(prob_vals[ix]) + ", ";
            cum_val += prob_vals[ix];
            cum_probs += std::to_string(cum_val) + ", ";
        }
        probs += std::to_string(prob_vals[prob_vals.size() - 1]);
        cum_probs += "1.0";
        std::string s = "\n  double " + gn.address + "_probs[] = " + "{" + probs + "};\n";
        s += "  double " + gn.address + "_cum_probs[] = " + "{" + cum_probs + "};\n\n";
        return s;
    }
};

template<>
struct translation<Categorical> {
    constexpr static bool value = true;
    node_spec operator()(graph_node<Categorical>& gn) {
        std::string params = gn.address + "_probs, " + gn.address + "_cum_probs";
        return (node_spec) {
            "Categorical",
            params,
            gn.address,
            "unsigned"
        };
    }
};

template<>
std::string node_type<Categorical>() { return "struct Categorical"; }

#endif  // GLPPL_DEFAULT_TRANSLATIONS_H