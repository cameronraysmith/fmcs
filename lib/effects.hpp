/**
 * @file 
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */


#ifndef EFFECTS_H
#define EFFECTS_H

#include <cstddef>
#include <functional>
#include <unordered_map>

#include <record.hpp>

template<typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> apply_record_effect(pp_t<I, O, Ts...>& f, record_interpretation interp) {
    return pp_t<I, O, Ts...>(
        [f, interp](record_t<DTypes<Ts...>>& _r, I i) -> O {
            _r.interp.push_front(interp);
            O value = f(_r, i);
            _r.interp.pop_front();
            return value;
        }
    );
}

/**
 * @brief Replays any subsequently passed record through the program.
 * 
 */
template<typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> replay(pp_t<I, O, Ts...>& f) {
    return apply_record_effect<I, O, Ts...>(f, RecordReplay());
}

template<typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> replay(pp_t<I, O, Ts...>&& f) {
    return replay<I, O, Ts...>(f);
}

/**
 * @brief Replaces probabilistic program-specified distributions with the distributions in any subsequently passed record
 * 
 */
template<typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> replace(pp_t<I, O, Ts...>& f) {
    return apply_record_effect<I, O, Ts...>(f, RecordReplace());
}

template<typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> replace(pp_t<I, O, Ts...>&& f) {
    return replace<I, O, Ts...>(f);
}

/**
 * @brief Blocks all sample sites. This could be useful for performing
 *  a stochastic optimization task.
 * 
 */
template<typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> block_sample(pp_t<I, O, Ts...>& f) {
    return apply_record_effect<I, O, Ts...>(f, RecordBlock<Sample>());
}

template<typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> block_sample(pp_t<I, O, Ts...>&& f) {
    return block_sample<I, O, Ts...>(f);
}

/**
 * @brief Blocks all observe sites. This could be useful for converting
 *  a probabilistic program into a proposal kernel.
 * 
 */
template<typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> block_observe(pp_t<I, O, Ts...>& f) {
    return apply_record_effect<I, O, Ts...>(f, RecordBlock<Obs>());
}

template<typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> block_observe(pp_t<I, O, Ts...>&& f) {
    return block_observe<I, O, Ts...>(f);
}

template<typename D, typename V, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> set_in_place_effect(pp_t<I, O, Ts...> f, std::string address, V value, node_interpretation interp) { 
    node_t<D> n;
    n.value = value;
    n.interp = interp;
    
    return pp_t<I, O, Ts...>(
        [=](record_t<DTypes<Ts...>>& _r, I i) -> O {
            if (!_r.map.contains(address)) _r.addresses.push_back(address);
            _r.map.insert_or_assign(address, n);
            return f(_r, i);
        }
    );
}

template<typename D, typename V, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> set_in_place_effect(pp_t<I, O, Ts...> f, std::unordered_map<std::string, V> map_, node_interpretation interp) { 
    return pp_t<I, O, Ts...>(
        [=](record_t<DTypes<Ts...>>& _r, I i) -> O {
            for (const auto& [address, value] : map_) {
                if (!_r.map.contains(address)) _r.addresses.push_back(address);
                _r.map.insert_or_assign(address, (node_t<D>) {.value = value, .interp = interp});
            }
            return f(_r, i);
        }
    );
}

/**
 * @brief Converts the site at the passed address, if it exists, into untraced randomnes.
 * 
 */
template<typename D, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> block(pp_t<I, O, Ts...>& f, std::string address) {     
    return pp_t<I, O, Ts...>(
        [=](record_t<DTypes<Ts...>>& _r, I i) -> O {
            if (!_r.map.contains(address)) {
                node_t<D> n;
                n.interp = NodeBlock();
                _r.map.insert_or_assign(address, n);
                return f(_r, i);
            } else {
                std::visit([&](auto& n){n.interp = NodeBlock();}, _r.map[address]);
                return f(_r, i);
            }
        }
    );
}

template<typename D, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> block(pp_t<I, O, Ts...>&& f, std::string address) { 
    return block<D, I, O, Ts...>(f, address);
}

/**
 * @brief Conditions the site at the address, if it exists, to be observed at the passed value.
 * 
 */
template<typename D, typename V, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> condition(pp_t<I, O, Ts...>& f, std::string address, V value) { 
    return set_in_place_effect<D, V, I, O, Ts...>(f, address, value, NodeCondition());
}

template<typename D, typename V, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> condition(pp_t<I, O, Ts...>&& f, std::string address, V value) { 
    return condition<D, V, I, O, Ts...>(f, address, value);
}

template<typename D, typename V, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> condition(pp_t<I, O, Ts...>& f, std::unordered_map<std::string, V> map_) { 
    return set_in_place_effect<D, V, I, O, Ts...>(f, map_, NodeCondition());
}

template<typename D, typename V, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> condition(pp_t<I, O, Ts...>&& f, std::unordered_map<std::string, V>&& map_) { 
    return condition<D, V, I, O, Ts...>(f, map_);
}

/**
 * @brief Replays the passed value through the model at the site at the passed address, if it exists.
 * 
 */
template<typename D, typename V, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> replay(pp_t<I, O, Ts...>& f, std::string address, V value) { 
    return set_in_place_effect<D, V, I, O, Ts...>(f, address, value, NodeReplay());
}

template<typename D, typename V, typename I, typename O, typename... Ts>
pp_t<I, O, Ts...> replay(pp_t<I, O, Ts...>&& f, std::string address, V value) { 
    return replay<D, V, I, O, Ts...>(f, address, value);
}

// template<typename I, typename O, typename... Ts>
// pp_t<I, O, Ts...> update_parameters(pp_t<I, O, Ts...>&f, param_store<Ts...>& db) {
//     return pp_t<I, O, Ts...>(
//         [&db](record_t<DTypes<Ts...>>& _r, I i) -> O {
//             for ([address, value] : db) {

//             }
//         }
//     );
// }

#endif  // EFFECTS_H
