/**
 * @file graph_distributions.hpp
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2023-01-21
 * 
 * @copyright Copyright (c) 2023 - present. Some rights reserved..
 */

#ifndef GLPPL_GRAPH_DISTRIBUTIONS_H
#define GLPPL_GRAPH_DISTRIBUTIONS_H

#include <functional>
#include <limits>
#include <optional>
#include <utility>

/**
 * @brief A minimal wrapper of a value that is tracked in a graph_ir.
 * 
 * This models the value as the distribution \f$p(v') = \delta( v' = v ) \f$,
 * where \f$v\f$ is the value passed to the constructor. Value nodes do not
 * have parents.
 * 
 * @tparam V The type of the value to be tracked
 */
template<typename V>
struct Value {
    V value;

    Value(V value) : value(value) {}

    template<typename RNG>
    V sample(RNG&) { return value; }

    double logprob(V value_) {
        return (value_ == value) ? 0.0 : -std::numeric_limits<double>::infinity();
    }

    std::string string() const {
        return "Value(value=" + stringify(value) + ")";
    }
};

#endif  // GLPPL_GRAPH_DISTRIBUTIONS_H