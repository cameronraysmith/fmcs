/**
 * @file particle.hpp
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-15
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <functional>
#include <memory>
#include <utility>
#include <variant>

#include <distributions.hpp>
#include <query.hpp>
#include <record.hpp>

#ifndef PARTICLE_H
#define PARTICLE_H

/**
 * @brief Type of values returned from filtering algorithms
 * 
 * @tparam O The output type of a probabilistic program
 * @tparam Ts The types of the distributions in the probabilistic program
 */
template<typename O, typename... Ts>
using FilterValueType = std::shared_ptr<record_collection_t<O, Ts...>>;


/**
 * @brief Computes \f$E_{r(a):\ r \sim D(r)}[f(r(a))]\f$, where \f$D(r)\f$ is the empirical distribution over records
 * and \f$ f: double -> double \f$.
 * 
 * @tparam Ts Types of distributions in the record
 * @param cache the empirical distribution over records
 * @param map_ the mapping function \f$f\f$
 * @param address the address over which to take the mean
 * @return double 
 */
template<typename O, typename... Ts>
double mean(FilterValueType<O, Ts...>& cache, std::function<double(double&&)>&& map_, std::string address) {
    double m = 0.0;
    auto get_value = [](const auto& n) -> double { return n.value; };
    double normalizer = 0.0;
    for (size_t ix = 0; ix != cache->_v.size(); ix++) {
        double weight = cache->prob_at(ix);
        m += map_(std::visit(get_value, cache->_v[ix].map[address])) * weight;
        normalizer += weight;
    }
    return m / normalizer;
}


/**
 * @brief Computes `mean` with \f$f = id \f$.
 * 
 */
template<typename O, typename... Ts>
double mean(FilterValueType<O, Ts...>& cache, std::string address) {
    return mean(cache, [](double&& x) { return x; }, address);
}

template<typename O, typename... Ts>
double variance(FilterValueType<O, Ts...>& cache, std::function<double(double&&)>&& map_, std::string address) {
    double var = 0.0;
    double m = mean(cache, std::forward<decltype(map_)>(map_), address);
    auto get_value = [](const auto& n) -> double { return n.value; };
    for (size_t ix = 0; ix != cache->_v.size(); ix++) {
        var += std::pow(map_(std::visit(get_value, cache->_v[ix].map[address])) - m, 2.0) * cache->prob_at(ix);
    }
    return var;
}


template<typename O, typename... Ts>
double variance(FilterValueType<O, Ts...>& cache, std::string address) {
    return variance(cache, [](double&& x){ return x; }, address);
}

template<typename O, typename... Ts>
double stddev(FilterValueType<O, Ts...>& cache, std::function<double(double&&)>&& map_, std::string address) { 
    return std::sqrt(variance(cache, std::forward<decltype(map_)>(map_), address)); 
}

template<typename O, typename... Ts>
double stddev(FilterValueType<O, Ts...>& cache, std::string address) {
    return stddev(cache, [](double&& x){ return x; }, address);
}


/**
 * @brief Computes \f$dist(r(a)) \sim D(r)\f$, where \f$dist(r(a))\f$ is the distribution object associated with the address \f$a\f$
 * and \f$D(r)\f$ is the empirical *unweighted* distribution over records.
 * 
 * @tparam Ts Types of distributions in the record
 * @param cache the empirical distribution over records
 * @param address the address over which to take the mean
 * @return std::vector<std::variant<Ts...>> 
 */
template<typename O, typename... Ts>
std::vector<std::variant<Ts...>> dist(FilterValueType<O, Ts...>& cache, std::string address) {
    std::vector<std::variant<Ts...>> d;
    for (auto& c : cache->_v) {
        d.push_back(
            std::visit([](const auto& n) -> std::variant<Ts...> { return n.distribution; }, c.map[address])
        );
    }
    return d;
}


/**
 * @brief Computes the score \f$ E_{r \sim D(r)}[\sum_{a: a\ observed} p(r(a))]\f$,
 * where \f$D(r)\f$ is the empirical *unweighted* distribution over records.
 * 
 * @tparam Ts Types of distributions in the record
 * @param cache the empirical distribution over records
 * @return double 
 */
template<typename O, typename... Ts>
double score(FilterValueType<O, Ts...>& cache) {
    double m = 0.0;
    for (auto& c : cache->_v) m += loglikelihood(c);
    return m / cache->_v.size();
}


/**
 * @brief Computes the (approximate) Akaike Information Criterion (AIC) of the model.
 * 
 * \f$AIC = 2 ( \sum_a dim(r(a)) - \sum_{a\ observed} \log p(r(a)) ) \f$, where
 * \f$r\f$ is the maximum likelihood record in the cache.
 * 
 * @tparam Ts Types of distributions in the record
 * @param cache the empirical distribution over records
 * @return double 
 */
template<typename O, typename... Ts>
double aic(FilterValueType<O, Ts...>& cache) {
    double a = 0.0;
    size_t argmax;
    double ll = -std::numeric_limits<double>::infinity();
    for (size_t ix = 0; ix != cache->_v.size(); ix++) {
        auto l = loglikelihood(cache->_v[ix]);
        if (l > ll) {
            argmax = ix;
            ll = l;
        }
    }
    size_t params = 0;
    for (auto& [_, n] : cache->_v[argmax].map) {
        params += std::visit([](auto& n ){ return output_dim<decltype(n.distribution)>::value; }, n);
    }
    return 2 * (params - ll);
}



#endif  // PARTICLE_H