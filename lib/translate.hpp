/**
 * @file translate.hpp
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.2.0
 * @date 2023-01-18
 * 
 * @copyright Copyright (c) 2023 - present. Some rights reserved..
 */

#ifndef GLPPL_TRANSLATE_H
#define GLPPL_TRANSLATE_H

#ifndef GLPPL_VERSION
#define GLPPL_VERSION "v0.3.0"
#endif

#include <algorithm>
#include <cctype>
#include <chrono>
#include <ctime>
#include <fstream>
#include <optional>
#include <string>
#include <type_traits>
#include <variant>

#include <distributions.hpp>
#include <graph.hpp>
#include <translate/default_translations.hpp>

;

/**
 * @brief Translate an unobserved node into corresponding ccyan sample semantics
 * 
 * @tparam D Type of distributino to be translated
 * @param gn the graph node to be translated
 * @param program_name the name of the program
 * @param observed is the node in the static set of ccyan observed nodes?
 * @return std::string 
 */
template<typename D>
std::string sample_statement(graph_node<D>& gn, std::string program_name, bool observed = false) {
    std::string obs_str = observed ? "observed" : "unobserved";
    static_assert(translation<D>::value, "graph node of this dist type has no defined translation");
    std::string s;
    node_spec spec = translation<D>()(gn);
    s += "  struct " + spec.dist_type + " " + spec.node_name + "_dist = {" + spec.dist_params + "};\n";
    s += "  " + spec.dist_sample_type + " " + spec.node_name + " = sample" + spec.dist_type + "(&" + spec.node_name + "_dist, " + program_name + "_RNG);\n";
    s += "  state." + obs_str + "." + spec.node_name + " = " + spec.node_name + ";\n";
    s += "  state." + obs_str + "." + spec.node_name + "_logprob = logprob" + spec.dist_type + "(&" + spec.node_name + "_dist, " + spec.node_name + ");\n";
    return s;
}

/**
 * @brief Translates a tracked value into corresponding ccyan statements
 * 
 * @tparam V The data type of the node. 
 * @param gn the graph node to be translated
 * @param program_name the name of the program
 * @return std::string 
 */
template<typename V>
std::string sample_statement(graph_node<Value<V>>& gn, std::string program_name, bool) {
    static_assert(translation<Value<V>>::value, "this value type has no defined translation");
    std::string s;
    node_spec spec = translation<Value<V>>()(gn);
    s += "  " + spec.dist_sample_type + " " + spec.node_name + " = " + spec.dist_params + ";\n";
    s += "  state.unobserved." + spec.node_name + " = " + spec.node_name + ";\n";
    s += "  state.unobserved." + spec.node_name + "_logprob = 0.0;\n";
    return s;
}

template<typename V>
std::string observe_statement(graph_node<Value<V>>& gn, std::string program_name, bool _) {
    return sample_statement(gn, program_name, _);
}

template<>
std::string sample_statement<Categorical>(graph_node<Categorical>& gn, std::string program_name, bool observed) {
    std::string obs_str = observed ? "observed" : "unobserved";
    std::string s;
    s += "  " + auxiliary_info<Categorical>()(gn);  // creates probs, cum_probs arrays
    node_spec spec = translation<Categorical>()(gn);
    s += "  struct " + spec.dist_type + " " + spec.node_name + "_dist = {" + spec.dist_params + "};\n";
    s += "  " + spec.dist_sample_type + " " + spec.node_name + " = sample" + spec.dist_type + "(&" + spec.node_name + "_dist, " + program_name + "_RNG);\n";
    s += "  state." + obs_str + "." + spec.node_name + " = " + spec.node_name + ";\n";
    s += "  state." + obs_str + "." + spec.node_name + "_logprob = logprob" + spec.dist_type + "(&" + spec.node_name + "_dist, " + spec.node_name + ");\n";
    return s;
}

/**
 * @brief Translates an observed node into corresponding ccyan observe semantics
 * 
 * @tparam D The type of node to be translated
 * @param gn the node to be translated
 * @param program_name the name of the program
 * @param observed is the node in the static set of ccyan observed nodes?
 * @return std::string 
 */
template<typename D>
std::string observe_statement(graph_node<D>& gn, std::string program_name, bool observed = true) {
    static_assert(translation<D>::value, "graph node of this dist type has no defined translation");
    std::string obs_str = observed ? "observed" : "unobserved";
    std::string s;
    node_spec spec = translation<D>()(gn);
    s += "  " + spec.dist_sample_type + " " + spec.node_name + " = state." + obs_str + "." + spec.node_name + ";\n";
    s += "  struct " + spec.dist_type + " " + spec.node_name + "_dist = {" + spec.dist_params + "};\n";
    s += "  state." + obs_str + "." + spec.node_name + "_logprob = logprob" + spec.dist_type + "(&" + spec.node_name + "_dist, " + "state." + obs_str + "." + spec.node_name + ");\n";
    return s;
}

template<>
std::string observe_statement<Categorical>(graph_node<Categorical>& gn, std::string program_name, bool observed) {
    std::string s;
    std::string obs_str = observed ? "observed" : "unobserved";
    s += "  " + auxiliary_info<Categorical>()(gn);  // creates probs, cum_probs arrays
    node_spec spec = translation<Categorical>()(gn);
    s += "  " + spec.dist_sample_type + " " + spec.node_name + " = state." + obs_str + "." + spec.node_name + ";\n";
    s += "  struct " + spec.dist_type + " " + spec.node_name + "_dist = {" + spec.dist_params + "};\n";
    s += "  state." + obs_str + "." + spec.node_name + "_logprob = logprob" + spec.dist_type + "(&" + spec.node_name + "_dist, " + "state." + obs_str + "." + spec.node_name + ");\n";
    return s;
}

namespace glppl_algos {
    struct likelihood_weighting {};
}

struct program_info {
    std::string program_name;
    std::optional<std::variant<glppl_algos::likelihood_weighting>> inference_algo = std::nullopt;
};

struct program_rep {
    std::string h_file;
    std::string c_file;
    std::string empty_main;
    std::string cmake_top;
    std::string cmake_lib;
};

void construct_program_frontmatter(program_info& info, std::string& program, std::time_t timestamp, bool is_header) {
    program += "/**\n";
    std::string ending = is_header ? ".h" : ".c";
    program += " * @file " + info.program_name + ending + "\n";
    program += " * @author glppl ";
    program += GLPPL_VERSION;
    program += " (info@dxdelta.com)\n";
    program += " * @version 0.1.0\n";
    ;
    auto timestamp_ = std::ctime(&timestamp);
    program +=  " * @date ";
    program += timestamp_;
    program += " * @copyright Copyright (c) above date - present DXDelta LLC. Some rights reserved. except as specified in license agreement. \n";
    program += " */\n\n";
}

void construct_program_h_includes(program_info& info, std::string& h_file) {
    std::string program_name_upper = info.program_name;
    std::transform(info.program_name.begin(), info.program_name.end(), program_name_upper.begin(), [](auto c){ return std::toupper(c); });
    std::string guard = "GENERATED_" + program_name_upper + "_H";
    h_file += "#ifndef " + guard + "\n#define " + guard + "\n\n";
    h_file += "#include <ccyan_distributions.h>\n\n";
}

void construct_program_c_includes(program_info& info, std::string& c_file) {
    c_file += "#include <ccyan_distributions.h>\n";
    c_file += "#include <ccyan_util.h>\n";
    c_file += "#include <" + info.program_name + ".h>\n\n";
}

template<typename... Ts>
void construct_program_structs(gr_pair<Ts...>& gr, program_info& info, std::string& h_file) {
    // unobserved rvs
    h_file += "struct " + info.program_name + "_unobserved {\n";
    for (auto& address : gr.g.addresses) {
        if (!std::visit([](auto&n){ return n.obs; }, gr.g.map[address])) {
            node_spec spec = std::visit([](auto& n){ return translate_node(n); }, gr.g.map[address]);
            h_file += "  " + spec.dist_sample_type + " " + spec.node_name + ";\n";
            h_file += "  double " + spec.node_name + "_logprob;\n";
        }
    }
    h_file += "};\n\n";

    // observed rvs
    h_file += "struct " + info.program_name + "_observed {\n";
    for (auto& address : gr.g.addresses) {
        if (std::visit([](auto&n){ return n.obs; }, gr.g.map[address])) {
            node_spec spec = std::visit([](auto& n){ return translate_node(n); }, gr.g.map[address]);
            h_file += "  " + spec.dist_sample_type + " " + spec.node_name + ";\n";
            h_file += "  double " + spec.node_name + "_logprob;\n";
        }
    }
    h_file += "};\n\n";

    // distributions
    h_file += "struct " + info.program_name + "_dists {\n";
    for (auto& address : gr.g.addresses) {
        node_spec spec = std::visit([](auto& n){ return translate_node(n); }, gr.g.map[address]);
        std::string dt = std::visit([](auto& n){ return node_type<decltype(n.distribution)>(); }, gr.g.map[address]);
        if (dt != GLPPL_NODE_TYPE_NULL) h_file += "  " + dt + " " + spec.node_name + ";\n";
    }
    h_file += "};\n\n";

    // combine all
    h_file += "struct " + info.program_name + "_state {\n";
    h_file += "  struct " + info.program_name + "_unobserved unobserved;\n";
    h_file += "  struct " + info.program_name + "_observed observed;\n";
    h_file += "  struct " + info.program_name + "_dists dists;\n";
    h_file += "};\n\n";
}

void construct_likelihood_weighting_declaration(program_info& info, std::string& h_file) {
    h_file += "void\n" + info.program_name + "_likelihood_weighting(\n";
    h_file += "  struct " + info.program_name + "_state state,\n"; 
    h_file += "  struct " + info.program_name + "_state * empirical_posterior,\n";
    h_file += "  double * weights,\n";
    h_file += "  double * cum_weights,\n";
    h_file += "  int num_samples,\n";
    h_file += "  struct rngstate * " + info.program_name + "_RNG\n);\n";
}

void construct_program_declarations(program_info& info, std::string& h_file) {
    // log p(state)
    h_file += "double\n" + info.program_name + "_accumulate_logprob(struct " + info.program_name + "_state * state_);\n\n";
    h_file += "double\n" + info.program_name + "_accumulate_loglikelihood(struct " + info.program_name + "_state * state_);\n\n";
    h_file += "double\n" + info.program_name + "_accumulate_loglatent(struct " + info.program_name + "_state * state_);\n\n";
    h_file += "struct " + info.program_name + "_state\n";
    h_file += info.program_name + "_standard(struct " + info.program_name + "_state state, struct rngstate * " + info.program_name + "_RNG);\n\n";

    // log p(x, z) conditioned on x and z
    h_file += "double\n" + info.program_name + "_score(struct " + info.program_name + "_state state);\n\n";

    // x ~ p(x, z) conditioned on z
    h_file += "struct " + info.program_name + "_state\n";
    h_file += info.program_name + "_predict(struct " + info.program_name + "_state state, struct rngstate * " + info.program_name + "_RNG);\n\n";

    // inference algorithm?
    if (info.inference_algo.has_value()) {
        std::visit(
            overloaded{
                [&info, &h_file](glppl_algos::likelihood_weighting) { construct_likelihood_weighting_declaration(info, h_file); }
            },
            info.inference_algo.value()
        );
    }
}

template<typename... Ts>
void construct_program_h_file(gr_pair<Ts...>& gr, program_info& info, std::string& h_file, std::time_t timestamp) {
    construct_program_frontmatter(info, h_file, timestamp, true);
    construct_program_h_includes(info, h_file);
    construct_program_structs(gr, info, h_file);
    construct_program_declarations(info, h_file);

    h_file += "#endif";
}

void construct_likelihood_weighting_definition(program_info& info, std::string& c_file) {
    c_file += "\n\nvoid\n" + info.program_name + "_likelihood_weighting(\n";
    c_file += "  struct " + info.program_name + "_state state,\n";
    c_file += "  struct " + info.program_name + "_state * empirical_posterior,\n";
    c_file += "  double * weights,\n  double * cum_weights,\n  int num_samples,\n  struct rngstate * " + info.program_name + "_RNG\n) {\n";
    c_file += "  for (int ix = 0; ix != num_samples; ++ix) {\n";
    c_file += "    empirical_posterior[ix] = " + info.program_name + "_standard(state, &" + info.program_name + "_RNG);\n";
    c_file += "    weights[ix] = " + info.program_name + "_accumulate_loglikelihood(&empirical_posterior[ix]);\n";
    c_file += "  }\n";
    c_file += "  double log_z = logsumexp(weights, num_samples);\n";
    c_file += "  normalize_in_place(weights, num_samples, log_z);\n";
    c_file += "  double_cumsum(weights, cum_weights, num_samples);\n";
    c_file += "}\n\n";
}

template<typename... Ts>
void construct_program_body(gr_pair<Ts...>& gr, program_info& info, std::string& c_file) {
    // accumulate_logprob
    c_file += "double\n" + info.program_name + "_accumulate_logprob(struct " + info.program_name + "_state * state_) {\n";
    c_file += "  struct " + info.program_name + "_state state = *state_;\n";
    c_file += "  double total_logprob = 0.0;\n";
    for (auto& address : gr.g.addresses) {
        std::string obs_string = std::visit([](auto& n){ return n.obs; }, gr.g.map[address]) ? "observed" : "unobserved";
        c_file += "  total_logprob += state." + obs_string + "." + address + "_logprob;\n";
    }
    c_file += "  return total_logprob;\n";
    c_file += "}\n\n";

    // accumulate_loglikelihood
    c_file += "double\n" + info.program_name + "_accumulate_loglikelihood(struct " + info.program_name + "_state * state_) {\n";
    c_file += "  struct " + info.program_name + "_state state = *state_;\n";
    c_file += "  double total_loglikelihood = 0.0;\n";
    for (auto& address : gr.g.addresses) {
        if (std::visit([](auto& n){ return n.obs; }, gr.g.map[address])) {
            c_file += "  total_loglikelihood += state.observed." + address + "_logprob;\n";
        }
    }
    c_file += "  return total_loglikelihood;\n";
    c_file += "}\n\n";

    // accumulate_loglatent
    c_file += "double\n" + info.program_name + "_accumulate_loglatent(struct " + info.program_name + "_state * state_) {\n";
    c_file += "  struct " + info.program_name + "_state state = *state_;\n";
    c_file += "  double total_loglatent = 0.0;\n";
    for (auto& address : gr.g.addresses) {
        if (!std::visit([](auto& n){ return n.obs; }, gr.g.map[address])) {
            c_file += "  total_loglatent += state.unobserved." + address + "_logprob;\n";
        }
    }
    c_file += "  return total_loglatent;\n";
    c_file += "}\n\n";

    // to which semantics should we translate?
    auto standard_bind = [&info](auto& n) -> std::string { 
        return n.obs ? observe_statement(n, info.program_name, n.obs) : sample_statement(n, info.program_name, n.obs);
    };
    auto score_bind = [&info](auto& n) -> std::string { 
        return observe_statement(n, info.program_name, n.obs);
    };
    auto post_predict_bind = [&info](auto& n) -> std::string { 
        return n.obs ? sample_statement(n, info.program_name, n.obs) : observe_statement(n, info.program_name, n.obs);
    };
    // \todo prior predict bind?

    // standard
    c_file += "struct " + info.program_name + "_state\n";
    c_file += info.program_name + "_standard(struct " + info.program_name + "_state state, struct rngstate * " + info.program_name + "_RNG) {\n";
    for (auto& address : gr.g.addresses) c_file += "\n" + std::visit(standard_bind, gr.g.map[address]);
    c_file += "\n  return state;\n";
    c_file += "}\n\n";

    // score
    c_file += "double\n";
    c_file += info.program_name + "_score(struct " + info.program_name + "_state state) {\n";
    for (auto& address : gr.g.addresses) c_file += "\n" + std::visit(score_bind, gr.g.map[address]);
    c_file += "  return " + info.program_name + "_accumulate_logprob(&state);\n";
    c_file += "}\n\n";

    // post predict
    c_file += "struct " + info.program_name + "_state\n";
    c_file += info.program_name + "_predict(struct " + info.program_name + "_state state, struct rngstate * " + info.program_name + "_RNG) {\n";
    for (auto& address : gr.g.addresses) c_file += "\n" + std::visit(post_predict_bind, gr.g.map[address]);
    c_file += "\n  return state;\n";
    c_file += "}";

    // inference algorithm?
    if (info.inference_algo.has_value()) {
        std::visit(
            overloaded{
                [&info, &c_file](glppl_algos::likelihood_weighting) { construct_likelihood_weighting_definition(info, c_file); }
            },
            info.inference_algo.value()
        );
    }
}

template<typename... Ts>
void construct_program_c_file(gr_pair<Ts...>& gr, program_info& info, std::string& c_file, std::time_t timestamp) {
    construct_program_frontmatter(info, c_file, timestamp, false);
    construct_program_c_includes(info, c_file);
    construct_program_body(gr, info, c_file);
}


void construct_program_empty_main(program_info& info, std::string& program, std::time_t timestamp) {
    program += "/**\n";
    program += " * @file " + info.program_name + "_main.c" + "\n";
    program += " * @author glppl and [Your name here] ";
    program += " (you@email.io)\n";
    program += " * @version X.Y.Z\n";
    ;
    auto timestamp_ = std::ctime(&timestamp);
    program +=  " * @date ";
    program += timestamp_;
    program += " * @copyright Copyright (c) above date - present, David Rushing Dewhurst and [your name here]. Released under the MIT license. \n";
    program += " */\n\n";

    program += "#include <" + info.program_name + ".h>\n\n";
    program += "int main(int argc, char ** argv) {\n";
    program += "  // your magic here...\n";
    program += "  return 0;\n";
    program += "}";
}

void construct_program_cmake_top(program_info& info, std::string& program) {

    program += "cmake_minimum_required(VERSION 3.20.0)\n";
    program += "set(CMAKE_C_STANDARD 99)\n";
    program += "set(CMAKE_C_STANDARD_REQUIRED True)\n\n";
    program += "project(generated_" + info.program_name + " VERSION 0.1.0)\n\n";

    program += "add_subdirectory(lib)\n";
    program += "add_subdirectory(include/ccyan)\n\n";

    program += "add_executable(" + info.program_name + ".out " + info.program_name + "_main.c)\n\n";

    program += "target_link_libraries(" + info.program_name + ".out PUBLIC ccyan)\n";
    program += "target_link_libraries(" + info.program_name + ".out PUBLIC " + info.program_name + ")\n";
    program += "target_link_libraries(" + info.program_name + ".out PUBLIC m)\n\n";

    program += "target_include_directories(" + info.program_name + ".out PUBLIC \"${PROJECT_BINARY_DIR}\" \"${PROJECT_SOURCE_DIR}/lib\")\n";               
}

void construct_program_cmake_lib(program_info& info, std::string& program) {
    program += "cmake_minimum_required(VERSION 3.20.0)\n";
    program += "set(CMAKE_C_STANDARD 99)\n";
    program += "set(CMAKE_C_STANDARD_REQUIRED True)\n\n";

    program += "project(" + info.program_name  + " VERSION 0.1.0)\n\n";

    program += "add_library(${PROJECT_NAME} " + info.program_name + ".c)\n";    
    program += "target_link_libraries(" + info.program_name + " m)  # link to libm\n";
    program += "target_link_libraries(" + info.program_name + " ccyan)\n\n";

    program += "target_include_directories(${PROJECT_NAME} PRIVATE ./)\n";
    program += "target_include_directories(${PROJECT_NAME} SYSTEM INTERFACE ./)\n\n";
    program += "target_include_directories(${PROJECT_NAME} PUBLIC \"${PROJECT_SOURCE_DIR}/include/ccyan\")\n";

    program += "target_compile_options(${PROJECT_NAME} PRIVATE -g -Os -Wconversion -Wsign-conversion)\n";
}

/**
 * @brief Translates a graph_ir into a sample-based graphical model library built on ccyan primitives.
 * 
 * Given a directed graphical model represented as a graph_ir, this function does multiple things:
 * 
 * + defines observed and unobserved structs that hold values of observed and unobserved random variables and their associated log probabilities
 * + defines a function to compute the joint log probability of the model
 * + defines functions that are equivalent to the following interpretations of the graphical model: (a) running it forward conditioned on observed data;
 *  (b) evaluating the joint log probability of the model conditioned on both observed and unobserved random variables; (c) predicting values of observed
 *  random variables conditioned on unobserved random variables. 
 * + creates associated build utilities (CMakeLists.txt files, blank canvas main file)
 * + \todo more functionality coming -- a work in progress
 * 
 * These functions are implemented using ccyan primitives. ccyan is a C99 library that depends only on libm and stdint. It does not dynamically allocate memory, 
 * making it safe for many low-resource or real-time applications.
 * 
 * @tparam Ts The distribution types in the graph_ir
 * @param gr a graph_ir and a record pair
 * @param info information about the generated program \todo more information -- a work in progress
 * @param file_ if not null, the directory in which to save the generated code relative to executable directory (e.g., "out/")
 * @return program_rep 
 */
template<typename... Ts>
program_rep construct_program(gr_pair<Ts...>& gr, program_info& info, std::optional<std::string> file_ = std::nullopt) {
    program_rep program;
    std::time_t timestamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    construct_program_h_file(gr, info, program.h_file, timestamp);
    construct_program_c_file(gr, info, program.c_file, timestamp);
    construct_program_empty_main(info, program.empty_main, timestamp);
    construct_program_cmake_top(info, program.cmake_top);
    construct_program_cmake_lib(info, program.cmake_lib);

    if (file_.has_value()) {
        std::ofstream file;

        file.open(file_.value() + "lib/" + info.program_name + ".h");
        file << program.h_file;
        file.close();

        file.open(file_.value() + "lib/" + info.program_name + ".c");
        file << program.c_file;
        file.close();

        file.open(file_.value() + info.program_name + "_main.c");
        file << program.empty_main;
        file.close();

        file.open(file_.value() + "CMakeLists.txt");
        file << program.cmake_top;
        file.close();

        file.open(file_.value() + "lib/CMakeLists.txt");
        file << program.cmake_lib;
        file.close();

    }
    return program;
}

#endif  // GLPPL_TRANSLATE_H