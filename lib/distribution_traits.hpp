/**
 * @file distribution_traits.hpp
 * @author David Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-16
 * 
 * @copyright Copyright (c) 2024, some rights reserved.
 * 
 */

#ifndef DISTRIBUTION_TRAITS_H
#define DISTRIBUTION_TRAITS_H

#include <string>
#include <utility>

/**
 * @brief How many input arguments does the largest constructor of the distribution have?
 * 
 */
template<typename D>
struct arity {
    static constexpr size_t value = 0;
};

/**
 * @brief What are the types passed as arguments to the largest constructor?
 * 
 */
template<typename D>
struct input_types {
    using value = std::tuple<>;
};

/**
 * @brief What are the dimensions of the output of calling `.sample(...)`?
 * 
 */
template<typename D>
struct output_dim {
    static constexpr size_t value = 1;
};

/**
 * @brief In what domain is the output?
 * 
 */
template<typename D>
struct output_domain;

/**
 * @brief Abstract base class that can be subclassed for creation of new distributions.
 * 
 * @tparam Impl the concrete implementation type
 * @tparam V the type of the distribution output;
 *  this should satisfy `static_assert(std::is_same_v<V, DSType<Impl>>)`
 */
template<typename Impl, typename V>
struct Distribution {

    template<typename RNG>
    V sample(RNG& rng) {
        return static_cast<Impl*>(this)->sample(rng);
    }

    double logprob(V value) {
        return static_cast<Impl*>(this)->logprob(value);
    }

    std::string string() const {
        return static_cast<Impl*>(this)->string();
    }

};

/**
 * @brief Used to determine the type of a distribution's sample return value at compile time.
 * 
 */
constexpr auto COMPILE_TIME_MOCK_PRNG = [](){ return 2022ULL; };

/**
 * @brief The return type of calling sample on a distribution object.
 * 
 * There is an injection from the space of distributions into the space of value types; the space
 * of value types is computed at compile time and is not needed as a template argument to sample calls.
 * 
 * @tparam D The type of distribution for which to compute the sample value type
 */
template<typename D>
using DSType = decltype(std::declval<D>().sample(COMPILE_TIME_MOCK_PRNG));



#endif  // DISTRIBUTION_TRAITS_H