/**  
 * @file
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#ifndef RECORD_H
#define RECORD_H

#include <cstddef>
#include <forward_list>
#include <functional>
#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <string>
#include <unordered_map>
#include <type_traits>
#include <utility>
#include <variant>

#include <domains.hpp>
#include <functional_util.hpp>
#include <distribution_traits.hpp>

struct NodeStandard {};
struct NodeReplay {};
struct NodeReplace {};
struct NodeCondition {};
struct NodeBlock {};
struct NodePropose {};
struct NodeParameter {};

/**
 * @brief Semantic instructions for actions to perform on the node.
 * 
 */
using node_interpretation = std::variant<
    NodeStandard,
    NodeReplay,
    NodeReplace,
    NodeCondition,
    NodeBlock,
    NodePropose,
    NodeParameter
>;


/**
 * @brief A fundamental data structure that includes address, distribution,
 * sampled value type, score, whether the value was observed, and a markov process
 * over interpretations
 * 
 * @tparam D the type of the distribution in the node; the value type is deduced
 *  from the distribution
 */
template<typename D>
struct node_t {
    D distribution;
    DSType<D> value;
    double logprob;
    bool obs;
    node_interpretation interp;
    node_interpretation last_interp;
    
    /**
     * @brief Returns a string representation of the node, not including interpretation.
     * 
     * @return std::string 
     */
    std::string string() const {
        std::string r = "";
        r += "node_t(distribution=" + distribution.string();
        r += ", value=" + stringify(value);
        r += ", logprob=" + std::to_string(logprob);
        r += obs ? ", obs=true" : ", obs=false";
        r += ")";
        return r;
    }
};

/**
 * @brief Creates a node with specified address by sampling value from corresponding
 *  distribution, then scoring it.
 * 
 */
template<typename D, typename RNG>
node_t<D> sample(D dist, RNG& rng) {
    auto value = dist.sample(rng);
    return node_t<D> {
        dist,
        value,
        dist.logprob(value),
        false,
        NodeStandard(),
        NodeStandard()
    };
}

/**
 * @brief Creates a node with specified address by scoring passed value from
 *  corresponding distribution.
 * 
 */
template<typename D, typename V>
node_t<D> observe(D dist, V value) {
    return node_t<D> {
        dist,
        value,
        dist.logprob(value),
        true,
        NodeCondition(),
        NodeCondition()
    };
}

template<typename... Ts>
using var_ret_t = std::variant<DSType<Ts>...>;

template<typename... Ts>
using var_node_t = std::variant<node_t<Ts>...>;

template<typename... Ts>
using node_map_t = std::unordered_map<std::string, var_node_t<Ts...>>;

struct Obs {};
struct Sample {};

struct RecordStandard {};
struct RecordReplay {};
struct RecordReplace {};
struct RecordRewrite {};

template<class Type>
struct RecordBlock;

template<> struct RecordBlock<Obs> {};
template<> struct RecordBlock<Sample> {};

/**
 * @brief Semantic instructions for operations to perform on the record.
 * 
 */
using record_interpretation = std::variant<
    RecordStandard,
    RecordReplay,
    RecordReplace,
    RecordRewrite,
    RecordBlock<Obs>,
    RecordBlock<Sample>
>;

template<typename... Ds>
using DTypes = Holder<Ds...>;

template<typename DTs>
struct record_t;

/**
 * @brief A fundamental data structure that holds a mapping from addresses to
 *  nodes, an insertion order, and a record-level interpretation.
 * 
 * @tparam Ts types of all nodes contained in the record. 
 */
template<typename... Ts>
struct record_t<DTypes<Ts...>> {
    node_map_t<Ts...> map;
    std::vector<std::string> addresses;
    std::forward_list<record_interpretation> interp;

    /**
     * @brief Clears the underlying map and insertion order.
     * 
     */
    void clear() {
        map.clear();
        addresses.clear();
    }

    /**
     * @brief Insert the node into the record at the specified address
     * 
     */
    template<typename... Others>
    void insert(std::string& address, var_node_t<Others...>& node) {
        if constexpr (std::is_same_v<DTypes<Ts...>, DTypes<Others...>>) {
            map.insert_or_assign(address, node);
        } else {
            map.insert_or_assign(
                address, 
                variant_supertype<DTypes<node_t<Others>...>, DTypes<node_t<Ts>...>>::convert(std::forward<var_node_t<Others...>>(node))
            );
        }
        addresses.push_back(address);
    }

    /**
     * @brief Copies data from each unobserved sample node from the passed record into this record
     * 
     * @param r the record from which to copy the data
     */
    template<typename... Others>
    void copy_unobserved_from_other(record_t<DTypes<Others...>>& r) {
        auto is_obs = [](auto& n) -> bool { return n.obs; };
        for (auto& address : r.addresses) {
            if (!std::visit(is_obs, r.map[address])) {
                if (!map.contains(address)) {
                    insert(address, r.map[address]);
                } else {
                    if constexpr (std::is_same_v<DTypes<Ts...>, DTypes<Others...>>) {
                        map.insert_or_assign(address, r.map[address]);
                    } else {
                        map.insert_or_assign(
                            address, 
                            variant_supertype<DTypes<node_t<Others>...>, DTypes<node_t<Ts>...>>::convert(std::forward<var_node_t<Others...>>(r.map[address]))
                        );
                    }
                }
            }
        }
        interp = r.interp;
    }

    /**
     * @brief Copies data from all nodes from the passed record into this record
     * 
     * @param r the record from which to copy the data
     */
    template<typename... Others>
    void copy_from_other(record_t<DTypes<Others...>>& r) {
        for (auto& address : r.addresses) {
            if (!map.contains(address)) {
                insert(address, r.map[address]);
            } else {
                if constexpr (std::is_same_v<DTypes<Ts...>, DTypes<Others...>>) {
                    map.insert_or_assign(address, r.map[address]);
                } else {
                    map.insert_or_assign(
                        address, 
                        variant_supertype<DTypes<node_t<Others>...>, DTypes<node_t<Ts>...>>::convert(std::forward<var_node_t<Others...>>(r.map[address]))
                    );
                }
            }
        }
        interp = r.interp;
    }
};

/**
 * @brief Samples a value into a node stored at the address by drawing a value from
 *  the specified distribution. Returns the underlying value of the created node.
 * 
 * @tparam D The type of distribution from which to sample
 * @tparam RNG The type of PRNG to use
 * @tparam Ts The distribution types contained in the passed record
 * @param r the record into which to sample
 * @param address the address at which to sample
 * @param dist the distribution from which to draw sample(s)
 * @param rng the PRNG to use for sampling
 * @return DSType<D> the sampled value
 */
template<typename D, typename RNG, typename... Ts>
DSType<D> sample(record_t<DTypes<Ts...>>& r, std::string address, D dist, RNG& rng) {
    if (r.interp.empty()) return _sample_with_node_interp<D, RNG, Ts...>(r, address, dist, rng);  // \todo when is this empty?
    return std::visit(
        overloaded(
            [&r, &address, &dist, &rng](RecordStandard) {return _sample_with_node_interp<D, RNG, Ts...>(r, address, dist, rng); },
            [&r, &address, &dist, &rng](auto) {return _sample_with_record_interp<D, RNG, Ts...>(r, address, dist, rng); }
        ),
        r.interp.front()
    );
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _sample_with_record_interp(record_t<DTypes<Ts...>>& r, std::string address, D dist, RNG& rng) {
    return std::visit(
        overloaded(
            [&r, &address, &dist, &rng](RecordReplay){ return _replay<D, RNG, Ts...>(r, address, dist, rng); },
            [&r, &address, &dist, &rng](RecordBlock<Sample>){ return _block_sample<D, RNG, Ts...>(r, address, dist, rng); },
            [&r, &address, &dist, &rng](RecordReplace){ return _replace<D, RNG, Ts...>(r, address, dist, rng); },
            [&r, &address, &dist, &rng](RecordRewrite){ return _rewrite<D, RNG, Ts...>(r, address, dist, rng); },
            [&r, &address, &dist, &rng](auto){ return _sample_with_node_interp<D, RNG, Ts...>(r, address, dist, rng); }
        ),
        r.interp.front()
    );
}

template<typename... Ds>
using data_types = unique_variant_t<var_ret_t<Ds...>>;

template<typename... Ds>
using param_store = std::unordered_map<std::string, data_types<Ds...>>;

template<typename D, typename RNG, typename... Ts>
DSType<D> _sample_with_node_interp(record_t<DTypes<Ts...>>& r, std::string address, D dist, RNG& rng) {
    if (!r.map.contains(address)) return _standard(r, address, dist, rng);
    auto get = [&r, &address, &dist, &rng](auto& node) -> data_types<Ts...> {
        return std::visit(
            overloaded(
                [&r, &address, &dist, &rng](NodeReplay){ return _replay(r, address, dist, rng); },
                [&r, &address, &dist, &rng](NodeReplace){ return _replace(r, address, dist, rng); },
                [&r, &address, &dist, &rng](NodeCondition){ return _condition(r, address, dist); },
                [&r, &address, &dist, &rng](NodeBlock){ return _block_sample(r, address, dist, rng); },
                [&r, &address, &dist, &rng](auto){ return _standard(r, address, dist, rng); }
            ),
            node.interp
        );
    };
    return std::get<DSType<D>>(std::visit(get, r.map[address]));
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _block_sample(record_t<DTypes<Ts...>>& r, std::string address, D dist, RNG& rng) {
    r.map.erase(address);
    return dist.sample(rng);
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _standard(record_t<DTypes<Ts...>>&r, std::string address, D dist, RNG& rng) {
    auto node = sample(dist, rng);
    if (!r.map.contains(address)) r.addresses.push_back(address);
    r.map.insert_or_assign(address, node);
    return node.value;
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _replay(record_t<DTypes<Ts...>>& r, std::string address, D dist, RNG& rng) {
    if (!r.map.contains(address)) return _standard<D, RNG, Ts...>(r, address, dist, rng);
    auto value = std::get<DSType<D>>(
        std::visit(
            [](auto& node) -> data_types<Ts...> { return node.value; },
            r.map[address]
        )
    );
    auto last_interp = std::visit(
        [](auto& node) -> node_interpretation { return node.last_interp; },
        r.map[address]
    );
    auto obs = std::visit(
        [](auto& node) -> bool { return node.obs; },
        r.map[address]
    );
    // note orig node can be originally only partially instantiated
    // *does not require* origin node to have dist
    auto node = node_t<D> { 
        .distribution=dist,
        .value=value,
        .logprob=dist.logprob(value),
        .obs=obs,
        .interp=last_interp,
        .last_interp=NodeReplay()
    };
    r.map.insert_or_assign(address, node);
    return node.value;
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _replace(record_t<DTypes<Ts...>>& r, std::string address, D orig_dist, RNG& rng) {
    if (!r.map.contains(address)) return _standard<D, RNG, Ts...>(r, address, orig_dist, rng);
    auto dist = std::get<D>(
        std::visit(
            [](auto& node) -> std::variant<Ts...> { return node.distribution; },
            r.map[address]
        )
    );
    return _standard<D, RNG, Ts...>(r, address, dist, rng);
}

template<typename D, typename RNG, typename... Ts>
DSType<D> _rewrite(record_t<DTypes<Ts...>>& r, std::string address, D orig_dist, RNG& rng) {
    if (!r.map.contains(address)) return _replay<D, RNG, Ts...>(r, address, orig_dist, rng);
    auto dist = std::get<D>(
        std::visit(
            [](auto& node) -> std::variant<Ts...> { return node.distribution; },
            r.map[address]
        )
    );
    return _replay<D, RNG, Ts...>(r, address, dist, rng);
}

template<typename D, typename... Ts> 
DSType<D> _condition(record_t<DTypes<Ts...>>& r, std::string address, D dist) {
    auto value = std::get<DSType<D>>(
        std::visit(
            [](auto& node) -> data_types<Ts...> { return node.value; },
            r.map[address]
        )
    );
    auto last_interp = std::visit(
        [](auto& node) -> node_interpretation { return node.last_interp; },
        r.map[address]
    );
    auto node = node_t<D> { 
        .distribution=dist,
        .value=value,
        .logprob=dist.logprob(value),
        .obs=true,
        .interp=NodeCondition(),
        .last_interp=last_interp
    };
    r.map.insert_or_assign(address, node);
    return node.value;
}

/**
 * @brief Scores the passed value against the distribution, storing the result in the
 *  record at the specified address.
 * 
 * @tparam D The type of distribution from which to sample
 * @tparam V The value type
 * @tparam Ts The distribution types contained in the passed record
 * @param r the record into which to store the scored value
 * @param address the address at which to store the scored value
 * @param dist the distribution against which to score values
 * @param v the value to be scored against the distribution
 * @return V the scored value
 */
template<typename D, typename V, typename... Ts>
V observe(record_t<DTypes<Ts...>>& r, std::string address, D dist, V value) {
    static_assert(std::is_same_v<DSType<D>, V>);
    if (r.interp.empty()) return _observe_with_node_interp<D, V, Ts...>(r, address, dist, value);
    return std::visit(
        overloaded(
            [&r, &address, &dist, &value](RecordStandard){ return _observe_with_node_interp<D, V, Ts...>(r, address, dist, value); },
            [&r, &address, &dist, &value](auto){ return _observe_with_record_interp<D, V, Ts...>(r, address, dist, value); }
        ),
        r.interp.front()
    );
}

template<typename V, typename... Ts>
struct ParamConstructor {

    record_t<DTypes<Ts...>>& r;
    std::string address;

    using type = typename V::type;

    ParamConstructor(record_t<DTypes<Ts...>>& r, std::string address)
        : r(r), address(address) {}

    type operator()(type value) {
        auto node = node_t<Parameter<V>> {
            Parameter<V>(value),
            value,
            0.0,
            false,
            NodeParameter(),
            NodeParameter()
        };
        if (!r.map.contains(address)) r.addresses.push_back(address);
        r.map.insert_or_assign(address, node);
        return value;
    }

};

template<typename V, typename... Ts>
ParamConstructor<V, Ts...> param(record_t<DTypes<Ts...>>& r, std::string address) {
    return ParamConstructor<V, Ts...>(r, address);
}

template<typename D, typename V, typename... Ts>
V _observe_with_record_interp(record_t<DTypes<Ts...>>& r, std::string address, D dist, V value) {
    return std::visit(
        overloaded(
            [&r, &address, &dist, &value](RecordBlock<Obs>){ return _block_observe<D, V, Ts...>(r, address, dist, value); },
            [&r, &address, &dist, &value](RecordReplace){ return _replace_observe<D, V, Ts...>(r, address, dist, value); },
            [&r, &address, &dist, &value](auto){ return _observe_with_node_interp<D, V, Ts...>(r, address, dist, value); }
        ),
        r.interp.front()
    );
}

template<typename D, typename V, typename... Ts>
V _replace_observe(record_t<DTypes<Ts...>>& r, std::string address, D orig_dist, V value) {
    if (!r.map.contains(address)) return _observe<D, V, Ts...>(r, address, orig_dist, value);
    auto dist = std::get<D>(
        std::visit(
            [](auto& node) -> std::variant<Ts...> { return node.distribution; },
            r.map[address]
        )
    );
    return _observe(r, address, dist, value);
}

template<typename D, typename V, typename... Ts>
V _observe_with_node_interp(record_t<DTypes<Ts...>>& r, std::string address, D dist, V value) {
    if (!r.map.contains(address)) return _observe(r, address, dist, value);
    auto get = [&r, &address, &dist, &value](auto& node) -> data_types<Ts...> {
        return std::visit(
            overloaded(
                [&r, &address, &dist, &value](NodeBlock){ return _block_observe(r, address, dist, value); },
                [&r, &address, &dist, &value](NodeReplace){ return _replace_observe(r, address, dist, value); },
                [&r, &address, &dist, &value](auto){ return _observe(r, address, dist, value); }
            ),
            node.interp
        );
    };
    return std::get<DSType<D>>(
        std::visit(get, r.map[address])
    );
}

template<typename D, typename V, typename... Ts>
V _observe(record_t<DTypes<Ts...>>& r, std::string address, D dist, V value) {
    if (!r.map.contains(address)) r.addresses.push_back(address);
    r.map.insert_or_assign(address, observe(dist, value));
    return value;
}

template<typename D, typename V, typename... Ts>
V _block_observe(record_t<DTypes<Ts...>>& r, std::string address, D, V value) {
    r.map.erase(address);
    return value;
}

/**
 * @brief Either samples a value from the specified distribution or observes a value against it, depending on
 * whether data or null is passed as `maybe_value`.
 * 
 * @tparam D The type of distribution from which to sample
 * @tparam RNG The type of PRNG to use
 * @tparam Ts The distribution types contained in the passed record
 * @param r the record into which to sample
 * @param address the address at which to sample
 * @param dist the distribution from which to draw sample(s)
 * @param rng the PRNG to use for sampling
 * @param maybe_value either the value to observe, or `std::nullopt` to signal that a sample is desired.
 * @return DSType<D> the sampled value
 */
template<typename D, typename RNG, typename... Ts>
DSType<D> sample(record_t<DTypes<Ts...>>& r, std::string address, D dist, RNG& rng, std::optional<DSType<D>> maybe_value) {
    if (maybe_value.has_value()) {
        return observe(r, address, dist, maybe_value.value());
    } else {
        return sample(r, address, dist, rng);
    }
}

template<template<typename, typename> class Map, typename V>
std::optional<V> get(Map<std::string, V>& map_, std::string& address) {
    if (!map_.contains(address)) return std::nullopt;
    return map_[address];
}

/**
 * @brief Either samples a value from the specified distribution or observes a value against it, depending on
 * whether there is data at the corresponding address in the passed `Map<std::string, DSType<D>>`.
 * 
 * @tparam Map map type with string keys and values of type mapped to by `dist.sample`.
 * @tparam D The type of distribution from which to sample
 * @tparam RNG The type of PRNG to use
 * @tparam Ts The distribution types contained in the passed record
 * @param r the record into which to sample
 * @param address the address at which to sample
 * @param dist the distribution from which to draw sample(s)
 * @param rng the PRNG to use for sampling
 * @param map_ map from addresses to values.
 * @return DSType<D> 
 */
template<template<typename, typename> class Map, typename D, typename RNG, typename... Ts>
DSType<D> sample(record_t<DTypes<Ts...>>& r, std::string address, D dist, RNG& rng, Map<std::string, DSType<D>>& map_) {
    return sample(r, address, dist, rng, get(map_, address));
}

/**
 * @brief Returns a string representation of the record. Does not display
 *  node- or record-level interpretations.
 * 
 * \todo reimplement as instance of generic fold
 * 
 */
template <typename... Ts>
std::string display(record_t<DTypes<Ts...>>& record) {
    auto disp = [](auto& node) -> std::string { return node.string(); };
    std::string s = "record_t(\n";
    for (const auto& address : record.addresses) {
        s += "  address=" + address + ", node=" + std::visit(disp, record.map[address]) + "\n";
    }
    s += ")";
    return s;
}

/**
 * @brief Computes the cumulative log probability of the record, \f$ \log p(x, z) \f$.
 * 
 */
template<typename... Ts>
double logprob(record_t<DTypes<Ts...>>& record) {
    auto get_lp = [](auto node) -> double { return node.logprob; };
    double lp = 0.0;
    for (const auto& [_, v] : record.map) {
        lp += std::visit(get_lp, v);
    }
    return lp;
}

/**
 * @brief Computes the scaled cumulative log-likelihood of the record, \f$ \sum_{a\ observed} \beta(\log p(r(a))) \f$. 
 * 
 * @tparam Ts The types of the distributions in the program
 * @param record the record for which to compute the log likelihood
 * @param scale \f$ \beta:\ double \rightarrow double \f$ scales the likelihood values
 * @return double 
 */
template<typename... Ts>
double loglikelihood(record_t<DTypes<Ts...>>& record, std::function<double(double)>&& scale) {
    auto get_ll = [](auto node) -> double { return node.obs ? node.logprob : 0.0; };
    double ll = 0.0;
    for (const auto& [_, v] : record.map) {
        ll += scale(std::visit(get_ll, v));
    }
    return ll;
}

/**
 * @brief Computes the scaled cumulative log-likelihood of the record on a per-address basis, \f$ \sum_{a\ observed} \beta(a, \log p(r(a))) \f$. 
 * 
 * @tparam Ts The types of the distributions in the program
 * @param record the record for which to compute the log likelihood
 * @param scale \f$ \beta:\ string \rightarrow double \rightarrow double \f$ scales the likelihood values
 * @return double 
 */
template<typename... Ts>
double loglikelihood(record_t<DTypes<Ts...>>& record, std::function<double(std::string&, double)>&& scale) {
    auto get_ll = [](auto node) -> double { return node.obs ? node.logprob : 0.0; };
    double ll = 0.0;
    for (const auto& [k, v] : record.map) {
        ll += scale(k, std::visit(get_ll, v));
    }
    return ll;
}

/**
 * @brief Computes the cumulative log-likelihood of the record, \f$ \sum_{a\ observed} \log p(r(a)) \f$. 
 * 
 * @tparam Ts The types of the distributions in the program
 * @param record the record for which to compute the log likelihood
 * @return double 
 */
template<typename... Ts>
double loglikelihood(record_t<DTypes<Ts...>>& record) {
    return loglikelihood(record, [](double x){ return x; });
}

/**
 * @brief Computes the cumulative log-latent of the record, \f$ \sum_{a\ unobserved} \log p(r(a)) \f$. 
 * 
 * @tparam Ts The types of the distributions in the program
 * @param record the record for which to compute the log latent
 * @return double 
 */
template<typename... Ts>
double loglatent(record_t<DTypes<Ts...>>& record) {
    auto get_ll = [](auto node) -> double { return node.obs ? 0.0 : node.logprob; };
    double ll = 0.0;
    for (const auto& [_, v] : record.map) {
        ll += std::visit(get_ll, v);
    }
    return ll;
}

/**
 * @brief A shorthand for probabilistic program type. Probabilistic programs are
 *  callables that take a single input of type I, output a single value of type O, 
 *  and contain an arbitrary positive number of distributions of type Ts...
 * 
 * To be well-formed, a probabilistic program should terminate with probability one.
 * 
 */
template<typename I, typename O, typename... Ts>
using pp_t = std::function<O(record_t<DTypes<Ts...>>&, I)>;

#endif  // RECORD_H
