/** 
 * @file 
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#ifndef PLATE_H
#define PLATE_H

#include <array>
#include <cassert>
#include <string>
#include <tuple>
#include <type_traits>

#include <cppitertools/zip.hpp>

#include <distribution_traits.hpp>
#include <record.hpp>

template<typename D>
struct plate_base_ {
    D dist;

    template<typename... Ts>
    plate_base_(Ts... ts) : dist(D(ts...)) {}
};

template<typename D, size_t N>
struct slice_base_ {
    std::array<D, N> dists;

    using tuple_type = typename input_types<D>::value;

    template<typename... Ts>
    slice_base_(std::shared_ptr<std::array<Ts, N>>... ts) {
        size_t n = 0;
        for (const tuple_type& t : iter::zip(*ts...)) {
            dists[n] = std::make_from_tuple<D>(t);
            ++n;
        }
    }
};

/**
 * @brief Represents \f$N\f$ iid random variables.
 * 
 * @tparam D The type of the underlying distribution
 * @tparam N The number of iid random variables
 */
template<typename D, size_t N>
struct static_plate : plate_base_<D> {

    template<typename... Ts>
    static_plate(Ts... ts) : plate_base_<D>(ts...) {}

    /**
     * @brief Draws \f$x_1,...,x_N \sim p(x|z)\f$. 
     * 
     * @tparam RNG The type of the pseudorandom number generator
     * @param rng The ppseudorandom number generator
     * @return std::shared_ptr<std::array<DSType<D>, N>> 
     */
    template<typename RNG>
    std::shared_ptr<std::array<DSType<D>, N>>
    sample(RNG& rng) {
        auto out = std::make_shared<std::array<DSType<D>, N>>();
        for (size_t ix = 0; ix != N; ix++) {
            out->operator[](ix) = this->dist.sample(rng);
        }
        return out;
    }

    /**
     * @brief Computes \f$\sum_{1\leq n \leq N} \log p(x_n | z)\f$.
     * 
     * @param value The \f$N\f$ random variables to score
     * @return double the log probability 
     */
    double logprob(std::shared_ptr<std::array<DSType<D>, N>> value) {
        double out = 0.0;
        for (size_t ix = 0; ix != N; ix++) out += this->dist.logprob(value->operator[](ix));
        return out;
    }

    std::string string() const {
        return "static_plate<" + this->dist.string() + ", " + std::to_string(N) + ">";
    }

};

/**
 * @brief Represents a vector of \f$N\f$ random variables
 * 
 * Each random variable is drawn from a distribution from the same family
 * but with different parent values. That is, \f$x_n \sim p(x|z_n)\f$
 * for \f$n = 1,...,N\f$.
 * 
 * @tparam D The type of the underlying distribution
 * @tparam N The number of random variables
 */
template<typename D, size_t N>
struct slice_plate : slice_base_<D, N> {

    template<typename... Ts>
    slice_plate(std::shared_ptr<std::array<Ts, N>>... ts) : slice_base_<D, N>(ts...) {}

    /**
     * @brief Samples \f$x_1,...,x_N \sim p(x_n | z_n)\f$.
     * 
     * @tparam RNG The type of the pseudorandom number generator
     * @param rng The pseudorandom number generator
     * @return std::shared_ptr<std::array<DSType<D>, N>> 
     */
    template<typename RNG>
    std::shared_ptr<std::array<DSType<D>, N>>
    sample(RNG& rng) {
        auto out = std::make_shared<std::array<DSType<D>, N>>();
        for (size_t ix = 0; ix != N; ++ix) {
            out->operator[](ix) = this->dists[ix].sample(rng);
        }
        return out;
    }

    /**
     * @brief Computes \f$ \sum_{1\leq n \leq N} \log p(x_n | z_n)\f$.
     * 
     * @param value The \f$N\f$ random variables to score
     * @return double the log probability
     */
    double logprob(std::shared_ptr<std::array<DSType<D>, N>> value) {
        double out = 0.0;
        for (size_t ix = 0; ix != N; ix++) out += this->dists[ix].logprob(value->operator[](ix));
        return out;
    }

    std::string string() const {
        return "slice_plate<" + std::to_string(N) + ">";
    }
};

#endif  // PLATE_H