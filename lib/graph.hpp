/**
 * \file graph.hpp
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.2.0
 * @date 2022-12-24
 * 
 * @copyright Copyright (c) 2022 - present. Some rights reserved..
 * 
 */

#ifndef GLPPL_GRAPH_H
#define GLPPL_GRAPH_H

#include <initializer_list>
#include <optional>
#include <string>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include <record.hpp>
#include <distributions/graph_distributions.hpp>

#define GLPPL_OPT_NULL "GLPPL_OPT(NULL)"

/**
 * @brief A fundamental data structure of which a graph intermediate representation is composed.
 * 
 * @tparam D 
 */
template<typename D>
struct graph_node {
    D distribution;
    DSType<D> value;
    bool obs;
    std::string address;
    std::vector<std::string> parents;
    std::vector<std::string> children;

    /**
     * @brief Returns the log probability of the value scored against the node's distribution
     * 
     * @param value the value to score
     * @return double 
     */
    double logprob(DSType<D> value) { return distribution.logprob(value); }

    /**
     * @brief Returns a sample from the node's distribution
     * 
     * @tparam RNG The type of PRNG used in the same
     * @param rng the PRNG used in the sample
     * @return DSType<D> 
     */
    template<typename RNG>
    DSType<D> sample(RNG& rng) { return distribution.sample(rng); }

    std::string string() const {
        std::string r = "graph_node(address=" + address + ", distribution=" + distribution.string();
        r += ", parents=" + stringify(parents) + ", children=" + stringify(children) + ")";
        return r;
    }
};

template<typename... Ts>
using var_graph_ret_t = std::variant<DSType<Ts>...>;

template<typename... Ts>
using var_graph_node_t = std::variant<graph_node<Ts>...>;

template<typename... Ts>
using graph_node_map_t = std::unordered_map<std::string, var_graph_node_t<Ts...>>;

using graph_repr_t = std::unordered_map<std::string, std::vector<std::string>>;

/**
 * @brief A graph intermediate representation of a causal model
 * 
 * @tparam Ts The distribution types contained in the probabilistic program
 */
template<typename... Ts>
struct graph_ir {
    graph_node_map_t<Ts...> map;
    std::vector<std::string> addresses;

    void clear() {
        map.clear();
        addresses.clear();
    }

    /**
     * @brief returns a human readable representation of the graph structure
     * 
    */
    graph_repr_t structure() {
        std::unordered_map<std::string, std::vector<std::string>> out;
        for (auto& [name, node] : map) {
            out[name] = std::visit([](auto& n){ return n.children; }, node);
        }   
        return out;
    }

    /**
    * @brief Checks equaity between two graph_irs.
    * 
    * graph_irs are equal if they contain the same distribution types and if they
    * have the same graphical structure.
    * 
    * @tparam Others the distribution types in the other graph_ir.
    * @param other the other graph_ir for which to check equality
    */
    template<typename... Others>
    bool operator==(graph_ir<Others...>& other) {
        if (!std::is_same<std::tuple<Ts...>, std::tuple<Others...>>::value) return false;
        return (this->structure() == other.structure());
    }

};

/**
 * @brief Returns a human-readable string representation of the graph
 * 
 * @tparam Ts The distribution types contained in the probabilistic program
 * @param g the graph_ir to print
 * @return std::string 
 */
template<typename... Ts>
std::string display(graph_ir<Ts...>& g) {
    auto disp = [](auto& node) -> std::string { return node.string(); };
    std::string s = "graph_ir(\n";
    for (const auto& address : g.addresses) {
        s += "  address=" + address + ", node=" + std::visit(disp, g.map[address]) + "\n";
    }
    s += ")";
    return s;
}

template<typename... Ts>
struct gr_pair {
    graph_ir<Ts...> g;
    record_t<DTypes<Ts...>> r;
    
    void clear() {
        g.clear();
        r.clear();
    }

    void insert_graph_node(std::string address, var_graph_node_t<Ts...> node) {
        g.map.insert_or_assign(address, node);
        g.addresses.push_back(address);
    }

    template<typename D>
    void transfer_distribution(std::string address) {
        D rd = std::get<node_t<D>>(r.map[address]).distribution;
        graph_node<D>& gn = std::get<graph_node<D>>(g.map[address]);
        gn.distribution = rd;
    }

    template<typename... Others>
    const bool operator==(gr_pair<Others...>& other) {
        return other.g == this->g;
    }

};

template<typename... Ts>
std::string display(gr_pair<Ts...>& gr) {
    std::string s = "graph: " + display(gr.g) + "\n";
    s += "record: " + display(gr.r);
    return s;
}

template<>
std::string stringify(std::string t) { return t; }

template<typename T>
std::string stringify(std::optional<T> t) { 
    return t.has_value() ? stringify(t.value()) : GLPPL_OPT_NULL;
}

template<typename V>
std::optional<std::string> extract_address(V v) { return std::nullopt; }

template<typename D>
std::optional<std::string> extract_address(graph_node<D>& v) { return v.address; }

std::vector<std::string> 
address_or_string_null(std::vector<std::optional<std::string>> maybe_strings) {
    std::vector<std::string> ret;
    for (auto& v : maybe_strings) {
        ret.push_back(stringify(v));
    }
    return ret;
}

template<typename V>
V extract_value(V v) { return v; }

template<typename D>
DSType<D> extract_value(graph_node<D>& v) {
    return v.value;
}

/**
 * @brief Finds and tracks parents/children of nodes involved in a sample or observe statement
 * 
 * @tparam D Distribution from which to sample or against which to observe
 * @tparam Ts Types of distributions in the probabilistic program
 */
template<typename D, typename... Ts>
struct graph_node_construct {

    gr_pair<Ts...>& gr;
    std::string address;
    bool obs;

    graph_node_construct(gr_pair<Ts...>& gr, std::string address, bool obs) 
        : address(address), gr(gr), obs(obs) {}

    template<typename... Args>
    void add_parents_children(graph_node<D>& node, Args&&... args) {
        node.address = address;
        node.obs = obs;
        auto parent_addresses = address_or_string_null({extract_address(args)...});
        for (auto& addr : parent_addresses) {
            node.parents.push_back(addr);
            std::visit([this](auto& n){ n.children.push_back(address); }, gr.g.map[addr]);
        }
    }
};

template<typename D, typename... Args>
D make_from_args(Args... args) {
    return std::make_from_tuple<D>(std::make_tuple(extract_value(args)...));
}

/**
 * @brief Creates a sample node in a graph_ir.
 * 
 * @tparam D Distribution type of the node
 * @tparam RNG PRNG used to sample from the distribution
 * @tparam Ts Types of distributions used in the probabilistic program
 */
template<typename D, typename RNG, typename... Ts>
struct graph_sample_node_construct : graph_node_construct<D, Ts...> {

    RNG& rng;

    graph_sample_node_construct(gr_pair<Ts...>& gr, std::string address, RNG& rng) 
        : rng(rng), graph_node_construct<D, Ts...>(gr, address, false) {}

    /**
     * @brief Constructs a sampled graph node based on passed parent nodes or values
     * 
     * The arity of this operator should have the same arity as one of the constructors of D or compilation
     * will fail. The passed arguments can either be of the base type required by the constructor of D or
     * can be of type graph_node_t<T> where T is one of the base types required by the constructor of D.
     * The arguments are evaluated from left to right.
     * 
     * @tparam Args Types of arguments to pass to the constructor of D
     * @param args arguments to pass to the constructor of D
     * @return graph_node<D> 
     */
    template<typename... Args>
    graph_node<D> operator()(Args&&... args) {
        graph_node<D> node;
        this->add_parents_children(node, args...);
        node.value = sample(this->gr.r, this->address, make_from_args<D>(args...), this->rng);
        this->gr.insert_graph_node(this->address, node);
        this->gr.template transfer_distribution<D>(this->address);
        return node;
    }
};

/**
 * @brief Creates an observe node in a graph_ir
 * 
 * @tparam D Distribution type of the node
 * @tparam V Type of value to score against the distribution
 * @tparam Ts Types of distributions used in the probabilistic program 
 */
template<typename D, typename V, typename... Ts>
struct graph_observe_node_construct : graph_node_construct<D, Ts...> {

    V value;

    graph_observe_node_construct(gr_pair<Ts...>& gr, std::string address, V value) 
        : value(value), graph_node_construct<D, Ts...>(gr, address, true) {}

    /**
     * @brief Constructs a observed graph node based on passed parent nodes or values
     * 
     * The arity of this operator should have the same arity as one of the constructors of D or compilation
     * will fail. The passed arguments can either be of the base type required by the constructor of D or
     * can be of type graph_node_t<T> where T is one of the base types required by the constructor of D.
     * The arguments are evaluated from left to right.
     * 
     * @tparam Args Types of arguments to pass to the constructor of D
     * @param args arguments to pass to the constructor of D
     * @return graph_node<D> 
     */
    template<typename... Args>
    graph_node<D> operator()(Args&&... args) {
        graph_node<D> node;
        this->add_parents_children(node, args...);
        node.value = observe(this->gr.r, this->address, make_from_args<D>(args...), this->value);
        this->gr.insert_graph_node(this->address, node);
        this->gr.template transfer_distribution<D>(this->address);
        return node;
    }
};

/**
 * @brief Prepares a sample node for construction and placement in the graph
 * 
 * @tparam D Type of the distribution of the node
 * @tparam RNG Type of the PRNG used to sample from the distribution
 * @tparam Ts Types of distributions in the probabilistic program
 * @param gr a graph_ir and a record
 * @param address the address at which to store the created node
 * @param rng the PRNG used to draw the samples
 * @return graph_sample_node_construct<D, RNG, Ts...> 
 */
template<typename D, typename RNG, typename... Ts>
graph_sample_node_construct<D, RNG, Ts...> 
sample_g(gr_pair<Ts...>& gr, std::string&& address, RNG& rng) {
    return graph_sample_node_construct<D, RNG, Ts...>(gr, address, rng);
}

/**
 * @brief Prepares an observe node for construction and placement in the graph
 * 
 * @tparam D Type of the distribution of the node
 * @tparam V Type of the value to score against the node
 * @tparam Ts Types of distributions in the probabilistic program 
 * @param gr a graph_ir and a record
 * @param address the address at which to store the created node 
 * @param value value to score against the distribution
 * @return graph_observe_node_construct<D, V, Ts...> 
 */
template<typename D, typename V, typename... Ts>
graph_observe_node_construct<D, V, Ts...> 
observe_g(gr_pair<Ts...>& gr, std::string&& address, V value) {
    return graph_observe_node_construct<D, V, Ts...>(gr, address, value);
}

/**
 * @brief Creates a value node and places it in the graph
 * 
 * @tparam V Type of value to be tracked
 * @tparam Ts Types of distributions in the probabilistic program
 * @param gr a graph_ir and a record
 * @param address the address at which to store the created node 
 * @param value the value to be tracked
 * @return graph_node<Value<V>> 
 */
template<typename V, typename... Ts>
graph_node<Value<V>> value_g(gr_pair<Ts...>& gr, std::string&& address, V value) {
    graph_node<Value<V>> node = {
        .distribution = Value<V>(value),
        .value = value,
        .obs = false,
        .address = address
    };
    gr.insert_graph_node(address, node);
    return node;
}

/**
 * @brief A shorthand for graph probabilistic program type. 
 *  Graph probabilistic programs are callables that take a single input of type I, 
 *  output a single value of type O, and contain an arbitrary positive number of distributions of
 *  type Ts...
 * 
 * @tparam I 
 * @tparam O 
 * @tparam Ts 
 */
template<typename I, typename O, typename... Ts>
using gpp_t = std::function<O(gr_pair<Ts...>&, I)>;

/**
 * @brief Product of a gr_pair and the output of a probabilistic program.
 * 
 * Intended for use with to_pp.
 * 
 * @tparam O 
 * @tparam Ts 
 */
template<typename O, typename... Ts>
struct gr_output {
    gr_pair<Ts...> gr;
    O o;
};

/**
 * @brief Convert a graph probabilistic program into a (record-based) probabilistic program
 * 
 * The record-based PP will maintain the semantics of the GPP, but will additionally return a
 *  gr_pair created during its execution.
 * 
 * @tparam I The input type of the GPP
 * @tparam O The output type of the GPP
 * @tparam Ts The distribution types contained in the GPP
 * @param f the GPP to convert to a PP
 * @return pp_t<I, gr_output<O, Ts...>, Ts...> 
 */
template<typename I, typename O, typename... Ts>
pp_t<I, gr_output<O, Ts...>, Ts...> to_pp(gpp_t<I, O, Ts...>& f) {
    return pp_t<I, gr_output<O, Ts...>, Ts...>(
        [&f](record_t<DTypes<Ts...>>& r, I i) {
            gr_pair<Ts...> gr;
            gr.r = r;  // copies into gr
            O o = f(gr, i);
            r.copy_from_other(gr.r);
            return gr_output<O, Ts...>{gr, o};
        }
    );
}

#endif  // GLPPL_GRAPH_H
