/**
 * \file
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2023 - present.
 * Some rights reserved.
 */

#include <unordered_map>
#include <variant>

#include <functional_util.hpp>
#include <record.hpp>

#ifndef UPDATE_H
#define UPDATE_H

/**
 * @brief A unordered map with value type equal to a sum type of input type
 * and one or more distribution types, parameterized by an update Policy.
 * 
 * The Policy parameter defines an injection 
 * \f$policy: \{D_i\}_{i \in I} \rightarrow \{D_j\}_{j \in J}\f$,
 * \f$J \subseteq I\f$.
 * This is implemented as `typename Policy<D>::type`,
 * which may be a bijection (e.g., when updating parameters in the same
 * distributional family) or may not (e.g., a normal approximation).
 * 
 * @tparam Policy encapsulates the types of distributions that can be returned
 *  by `get`. Must define `type` with a using-declaration.
 * @tparam I The input type
 * @tparam Ds The universe of allowable distribution types
 */
template<
    template<typename> class Policy,
    typename Input,
    typename... Ds
>
struct typed_map {
    using value_t = unique_variant_t<typename cat<std::variant<Ds...>, data_types<Ds...>, Input>::type>;

    std::unordered_map<std::string, value_t> map_;

    bool contains(std::string name) { return map_.contains(name); }
    
    void insert_or_assign(std::string name, value_t obj) { map_.insert_or_assign(name, obj); }

    /**
     * @brief Is the distribution at the address transformed via a Policy?
     * 
     * @tparam D the original distribution
     * @param address the address to check
     */
    template<typename D>
    bool is_transformed(std::string address) {
        return std::holds_alternative<typename Policy<D>::type>(map_[address]);
    }

    /**
     * @brief Return a distribution according to a Policy.
     * 
     * @tparam D distribution type intended in original program semantics
     * @param name the address to query
     * @return Policy<D>::type an object with type specified by the policy.
     */
    template<typename D>
    typename Policy<D>::type extract(std::string name) {
        return std::get<typename Policy<D>::type>(map_[name]);
    }

    /**
     * @brief Return a value or distribution according to original value
     * 
     * @tparam V input or distribution type
     * @param name the address to query
     * @return V
     */
    template<typename V>
    V extract_value(std::string name) {
        return std::get<V>(map_[name]);
    }

};

/**
 * @brief Alias to probabilistic programs that take typed_map references
 * as inputs.
 * 
 * @tparam Policy encapsulates the types of distributions that can be returned
 *  by `get`. Must define `type` with a using-declaration.
 * @tparam I the input type
 * @tparam O the output type
 * @tparam Ts The universe of allowable distribution types
 */
template<
    template<typename> class Policy,
    typename I,
    typename O,
    typename... Ts
>
using upp_t = pp_t<typed_map<Policy, I, Ts...>&, O, Ts...>;

/**
 * @brief *Experimental* base class of `typed_map`-based update logic.
 * 
 * @tparam Impl Implementation template template
 * @tparam Policy encapsulates the types of distributions that can be returned
 *  by `get`. Must define `type` with a using-declaration.
 * @tparam QueryResult the type returned from the desired queryer.
 * @tparam I The input type
 * @tparam Ts The universe of allowable distribution types
 */
template<
    template <
        template<typename> class,
        typename,
        typename,
        typename...
    > class Impl,
    template<typename> class Policy,
    typename QueryResult,
    typename I,
    typename O,
    typename... Ts
>
struct Update {
    using typed_map_ = typed_map<Policy, I, Ts...>;

    upp_t<Policy, I, O, Ts...>& f;

    /**
     * @brief Calls derived implementation.
     * 
     * @param result the result from a queryer.
     * @return typed_map<Policy, I, Ts...> 
     */
    typed_map_ operator()(QueryResult& result) {
        return static_cast<Impl<Policy, QueryResult, I, O, Ts...>*>(this)->operator()(result);
    }
};

/**
 * @brief Factory function for Updater.
 * 
 * @param f A `upp_t` for which updates may be performed.
 * @return Update<Impl, Policy, QueryResult, I, O, Ts...> 
 */
template<
    template <
        template<typename> class,
        typename,
        typename,
        typename...
    > class Impl,
    template<typename> class Policy,
    typename QueryResult,
    typename I,
    typename O,
    typename... Ts
>
Update<Impl, Policy, QueryResult, I, O, Ts...>
update(upp_t<Policy, I, O, Ts...>& f) {
    return Update<Impl, Policy, QueryResult, I, O, Ts...>(f);
}

/**
 * @brief Sample a value from the addressed distribution in the `typed_map`
 * 
 * For example,
 * ```
 * auto loc = sample_u<Normal>(r, "loc", input, rng);
 * ```
 * corresponds to the semantics "sample the value for "loc" from the distribution contained
 * in the map; my prior belief is that that distribution is Normal".
 * 
 */
template<typename D, typename RNG, template<typename> class Policy, typename I, typename... Ts>
DSType<D> sample_u(
    record_t<DTypes<Ts...>>& r, 
    std::string address, 
    typed_map<Policy, I, Ts...>& map_, 
    RNG& rng
) {
    if (map_.contains(address) && !(map_.template is_transformed<D>(address))) {
        return sample(r, address, map_.template extract_value<D>(address), rng);
    } else {
        auto raw_result = sample(r, address, map_.template extract<D>(address), rng);
        // and backtransform to the original domain, required by program logic
        return mapping<
            typename output_domain<D>::value, 
            typename output_domain<
                typename Policy<D>::type
            >::value
        >::backward(raw_result);
    }
}

/**
 * @brief Observe a value from data contained in the `typed_map`.
 */
template<typename V, typename D, template<typename> class Policy, typename I, typename... Ts>
V observe_u(
    record_t<DTypes<Ts...>>& r, 
    std::string address, 
    D dist, 
    typed_map<Policy, I, Ts...>& map_
) {
    return observe(r, address, dist, map_.template extract_value<V>(address));
}

/**
 * @brief A filtering algorithm that operates on typed_map objects.
 * 
 * This algorithm performs the following steps:
 * 
 * + Samples from the posterior distribution using the Inference algorithm
 * + Constructs a view of the posterior using the Queryer
 * + Using the distribution mapping defined by the Policy, updates to a new prior
 *  distribution as specified by the Impl.
 * 
 * @tparam Impl the prior -> posterior update implementation
 * @tparam Policy the injection between distributions
 * @tparam Queryer the view of the posterior distribution
 * @tparam Inference the algorithm for drawing samples from the posterior distribution
 * @tparam I the input type of the updateable probabilistic program
 * @tparam O the output type of the updateable probabilistic program
 * @tparam Ts the universe of possible types in the updateable probabilistic program
 */
template<
    template <
        template<typename> class,
        typename,
        typename,
        typename...
    > class Impl,
    template<typename> class Policy,
    class Queryer,
    class Inference,
    typename I,
    typename O,
    typename... Ts
>
struct UpdateFilter {

    using QueryResult = decltype(std::declval<Queryer>().emit());
    using typed_map_ = typed_map<Policy, I, Ts...>;

    upp_t<Policy, I, O, Ts...>& f;
    Queryer& q;
    Inference& infer;
    Update<Impl, Policy, QueryResult, I, O, Ts...> updater;

    UpdateFilter(
        upp_t<Policy, I, O, Ts...>& f,
        Queryer& q,
        Inference& infer
    ) : f(f), q(q), infer(infer), updater(update<Impl, Policy, QueryResult>(f)) {}

    typed_map_& step(typed_map_& m) {
        auto res = infer(m);
        m = updater(res);
        q.clear();
        return m;
    }

};

/**
 * @brief Factory function for UpdateFilter
 *
 */
template<
    template <
        template<typename> class,
        typename,
        typename,
        typename...
    > class Impl,
    template<typename> class Policy,
    class Queryer,
    class Inference,
    typename I,
    typename O,
    typename... Ts
>
UpdateFilter<Impl, Policy, Queryer, Inference, I, O, Ts...>
filter(upp_t<Policy, I, O, Ts...>& f, Queryer& q, Inference& infer) {
    return UpdateFilter<Impl, Policy, Queryer, Inference, I, O, Ts...>(f, q, infer);
}

#endif  // UPDATE_H