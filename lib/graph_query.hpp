/**
 * \file graph_query.hpp
 * @author David Rushing Dewhurst (drd@davidrushingdewhurst.com)
 * @brief 
 * @version 0.2.0
 * @date 2023-01-07
 * 
 * @copyright Copyright (c) 2023 - present. Some rights reserved..
 * 
 */

#ifndef GLPPL_EXTRACT_H
#define GLPPL_EXTRACT_H

#include <algorithm>
#include <cmath>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include <distributions.hpp>
#include <query.hpp>

#include <graph.hpp>

/**
 * @brief Queries for the maximum *a posteriori* structure.
 * 
 * The result of calling emit on the resulting queryer is the \f$\arg \max_{gr\_output} p(gr\_output | data)\f$,
 * where the posterior over gr_output is constructed by the user-selected inference
 * algorithm. More mathematically, suppose the graph probabilistic program factors as
 * \f$ p(x, z, g) = p(x|z, g) p(z, g)\f$, where \f$x\f$ is observed random variables, \f$x\f$ are unobserved
 * random variables, and \f$g\f$ are random variables determining the structure of the graph associated with
 * each sample. The resulting queryer returns \f$\arg \max_{g, z} p(g, z|x)\f$.
 * 
 * @tparam O The output type of the graph probabilistic program
 * @tparam Ts The types of distributions contained within the probabilistic program
 * @return auto An Optimizer instance
 */
template<typename O, typename... Ts>
auto map_structure() {
    return Optimizer<gr_output<O, Ts...>, gr_output<O, Ts...>, Ts...>(
        [](record_t<DTypes<Ts...>>&, gr_output<O, Ts...>& o, double&){ return o; },
        [](record_t<DTypes<Ts...>>& r, gr_output<O, Ts...>&, double&){ 
            return logprob(r);
        }
    );
}

/**
 * @brief Queries for the maximum likelihood structure.
 * 
 * The result of calling emit on the resulting queryer is the \f$\arg \max_{gr\_output} p(data | gr\_output)\f$,
 * where the posterior over gr_output is constructed by the user-selected inference
 * algorithm. More mathematically, suppose the graph probabilistic program factors as
 * \f$ p(x, z, g) = p(x|z, g) p(z, g)\f$, where \f$x\f$ is observed random variables, \f$x\f$ are unobserved
 * random variables, and \f$g\f$ are random variables determining the structure of the graph associated with
 * each sample. The resulting queryer returns \f$\arg \max_{g, z} p(x|g, z)\f$.
 * 
 * @tparam O The output type of the graph probabilistic program
 * @tparam Ts The types of distributions contained within the probabilistic program
 * @return auto An Optimizer instance
 */
template<typename O, typename... Ts>
auto mle_structure() {
    return Optimizer<gr_output<O, Ts...>, gr_output<O, Ts...>, Ts...>(
        [](record_t<DTypes<Ts...>>&, gr_output<O, Ts...>& o, double&){ return o; },
        [](record_t<DTypes<Ts...>>& r, gr_output<O, Ts...>&, double&){ 
            return loglikelihood(r);
        }
    );
}

#endif  // GLPPL_EXTRACT_H