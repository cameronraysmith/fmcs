/** 
 * \file generic.hpp
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#ifndef IS_GENERIC_H
#define IS_GENERIC_H

#include <effects.hpp>
#include <query.hpp>
#include <record.hpp>
#include <inference/inference.hpp>
#include <inference/proposal.hpp>

template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct ImportanceSampling;

template<typename I, typename O, typename... Ts>
struct inference_state<ImportanceSampling, I, O, Ts...> {
    exog_proposal_t<I, Ts...> proposal;
};

/**
 * @brief Importance sampling using an arbitrary user-defined proposal distribution.
 * 
 * Suppose the probabilistic program factors as \f$p(x, z) = p(x|z)p(z)\f$, while the proposal distribution
 * is given by \f$q(z|x)\f$. This method proposes values \f$z \sim q(z|x)\f$ and computes weights as
 * \f$ \log w = \log p(x,z) - \log q(z|x)\f$. 
 * 
 * @tparam I The input type of the probabilistic program
 * @tparam O The output type of the probabilistic program
 * @tparam V The type of the address(es) to be queried
 * @tparam Q The class template of the queryer
 * @tparam Ts The distribution types in the probabilistic program
 */
template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct ImportanceSampling : Inference<ImportanceSampling, I, O, V, Q, Ts...> {

    void step(record_t<DTypes<Ts...>>& r, I& input) {
        double log_q = this->state.proposal(r, input);
        r.interp.push_front(RecordReplay());
        O o = this->f(r, input);
        if (!r.interp.empty()) r.interp.pop_front();
        double log_p = logprob(r);
        this->queryer.update(
            r,
            o,
            log_p - log_q,
            this->opts
        );
        r.clear();
    }

    V operator()(I& input, exog_proposal_t<I, Ts...>& p) {
        record_t<DTypes<Ts...>> r;
        this->state.proposal = p;
        for (size_t n = 0; n != this->opts._num_iterations; n++) step(r, input);
        return this->queryer.emit();
    }
};

template<>
struct has_proposal<ImportanceSampling> {
    using type = Exog;
};

#endif  // IS_GENERIC_H