/** 
 * \file likelihood.hpp
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#ifndef LW_H
#define LW_H

#include <effects.hpp>
#include <record.hpp>
#include <query.hpp>
#include <inference/inference.hpp>

/**
 * @brief Likelihood weighting importance sampling, using the prior as the proposal
 * 
 * Suppose the probabilistic program factors as \f$p(x,z) = p(x|z)p(z)\f$. 
 * This method samples values \f$z \sim p(z)\f$. The weight of each sample is given by 
 * \f$\log w = \log p(x|z)\f$.
 * 
 * @tparam I The input type of the probabilistic program
 * @tparam O The output type of the probabilistic program
 * @tparam V The type of the address(es) to be queried
 * @tparam Q The class template of the queryer
 * @tparam Ts The distribution types in the probabilistic program
 */
template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct LikelihoodWeighting : Inference<LikelihoodWeighting, I, O, V, Q, Ts...> {
    void step(record_t<DTypes<Ts...>>& r, I& input) {
        O o = this->f(r, input);
        this->queryer.update(r, o, loglikelihood(r), this->opts);
        r.clear();
    }
    V operator()(I& input) {
        record_t<DTypes<Ts...>> r;
        for (size_t n = 0; n != this->opts._num_iterations; n++) step(r, input);
        return this->queryer.emit();
    }
};

#endif  // LW_H