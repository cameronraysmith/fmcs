/** 
 * \file 
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#ifndef METRO_BASE_H
#define METRO_BASE_H

#include <algorithm>
#include <iostream>
#include <cstddef>
#include <cmath>
#include <functional>
#include <memory>
#include <optional>
#include <random>
#include <unordered_map>
#include <vector>

#include <distributions.hpp>
#include <record.hpp>
#include <effects.hpp>
#include <query.hpp>

#include <inference/inference.hpp>
#include <inference/proposal.hpp>

/**
 * @brief Log acceptance probability for use when proposing from the prior
 * 
 * @tparam Ts the distribution types contained in each record
 * @param orig_r the current record
 * @param new_r the proposed new record
 * @return double log acceptance probability
 */
template<typename... Ts>
double log_accept_prior(record_t<DTypes<Ts...>>& orig_r, record_t<DTypes<Ts...>>& new_r) {
    double new_ll = loglikelihood(new_r);
    double old_ll = loglikelihood(orig_r);
    return new_ll - old_ll;
}

template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct AncestorMetropolis;

template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct GenericMetropolis;

template<typename I, typename O, typename... Ts> 
struct inference_state<AncestorMetropolis, I, O, Ts...> {
    Uniform u;
    record_t<DTypes<Ts...>> r;
    std::minstd_rand rng;

    inference_state(unsigned seed=2022) : rng(seed) {}

    void seed(unsigned seed) {
        rng = std::minstd_rand(seed);
    }

    double log_u() {
        return std::log(u.sample(rng));
    }
};

template<typename I, typename O, typename... Ts> 
struct inference_state<GenericMetropolis, I, O, Ts...> 
    : inference_state<AncestorMetropolis, I, O, Ts...> {

    pp_t<I, O, Ts...> f;
    endog_proposal_t<Ts...> proposal;
    endog_proposal_t<Ts...> replayed_proposal;

};

/**
 * @brief Metropolis Hastings using the prior distribution as the proposal.
 * 
 * Suppose the generative model factors as \f$ p(x,z) = p(x|z)p(z) \f$.
 * The acceptance ratio is \f$ \log \alpha = \log p(x|z') - \log p(x|z) \f$, where
 * \f$ z' \sim p(z) \f$ is the new draw from the prior, and \f$ z \sim p(z) \f$ is the
 * old/existing draw from the prior. Each weight passed to the queryer is \f$ \log w = 0 \f$.
 * 
 * @tparam I The input type of the program on which to perform inference
 * @tparam O The output type of the program on which to perform inference
 * @tparam V The type emitted by the queryer
 * @tparam Q The queryer type
 * @tparam Ts The distribution types used in the probabilistic program
 */
template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct AncestorMetropolis : Inference<AncestorMetropolis, I, O, V, Q, Ts...> {
    
    void step(record_t<DTypes<Ts...>>& r, I& input) {
        record_t<DTypes<Ts...>> new_r;
        O o = this->f(new_r, input);
        double log_accept = log_accept_prior(this->state.r, new_r);
        if (this->state.log_u() < log_accept) this->state.r = new_r;
        if ((this->opts._num_complete > this->opts._burn) && (this->opts._num_complete % this->opts._thin == 0)) {
            this->queryer.update(this->state.r, o, 0.0, this->opts);
        }
    }

    V operator()(I& input) {
        this->f(this->state.r, input);
        while (this->opts._num_complete != this->opts._num_iterations) {
            step(this->state.r, input);
            this->opts._num_complete++;
        }
        return this->queryer.emit();
    }
};

/**
 * @brief Generic Metropolis-Hastings algorithm with user-specified proposal distribution.
 * 
 * Suppose the probabilistic program factors as \f$p(x, z) = p(x|z)p(z)\f$ and the proposal distribution
 * takes the form \f$q(z'|z)\f$. The log acceptance ratio is computed as
 * \f$ \log \alpha = [\log p(x, z') - \log q(z' | z)] - [\log p(x, z) - \log q(z | z')]\f$, where
 * \f$z' \sim q(z'|z)\f$ are the newly generated unobserved rvs.
 * The proposal distribution need not be symmetric.
 * 
 * @tparam I The input type of the probabilistic program
 * @tparam O The output type of the probabilistic program
 * @tparam V The type of the value to be queried
 * @tparam Q The queryer class template
 * @tparam Ts The types of the distributions in the probabilistic program
 */
template<typename I, typename O, typename V, template<class, class, class...> class Q, typename... Ts>
struct GenericMetropolis : Inference<GenericMetropolis, I, O, V, Q, Ts...> {

    void step(record_t<DTypes<Ts...>>&, I& input) {
        // log q(z' | z)
        record_t<DTypes<Ts...>> r_new;
        double log_q_zprime_given_z = this->state.proposal(r_new, this->state.r);
        // log p(x, z')
        O o = this->state.f(r_new, input);
        double log_p_x_zprime = logprob(r_new);
        // log q(z | z')
        record_t<DTypes<Ts...>> current_r_copy = this->state.r;
        double log_q_z_given_zprime = this->state.replayed_proposal(current_r_copy, r_new);
        // log p(x, z)
        double log_p_x_z = logprob(this->state.r);

        double log_accept = log_p_x_zprime + log_q_z_given_zprime - log_p_x_z - log_q_zprime_given_z;
        if (this->state.log_u() < log_accept) this->state.r = r_new;  // \todo move?
        if ((this->opts._num_complete > this->opts._burn) && (this->opts._num_complete % this->opts._thin == 0)) {
            this->queryer.update(this->state.r, o, 0.0, this->opts);
        }
    }

    /**
     * @brief Runs the inference algorithm
     * 
     * @param input The input to the probabilistic program
     * @param proposal The proposal kernel. It should return \f$log q(z'|z)\f$ i.e. loglatent(new_r).
     * @return V 
     */
    V operator()(I& input, endog_proposal_t<Ts...>& proposal) {
        // populate an initial trace
        this->f(this->state.r, input);
        auto replayed_proposal = replay(proposal);
        auto replayed_f = replay(this->f);
        this->state.f = replayed_f;
        this->state.proposal = proposal;
        this->state.replayed_proposal = replayed_proposal;

        while (this->opts._num_complete != this->opts._num_iterations) {
            step(this->state.r, input);
            this->opts._num_complete++;
        }
        return this->queryer.emit();
    }
};

template<>
struct has_proposal<GenericMetropolis> {
    using type = Endog;
};

#endif  // METRO_BASE_H
