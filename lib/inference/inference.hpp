/** 
 * \file inference.hpp
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */

#ifndef INFERENCE_H
#define INFERENCE_H

#include <type_traits>

#include <effects.hpp>
#include <record.hpp>
#include <query.hpp>

struct No {};
struct Exog {};
struct Endog {};

/**
 * @brief Will the passed class template have an associated proposal distribution?
 * 
 * @tparam C The class template to check
 */
template<template<class, class, class, template<class, class, class...> class, typename...> class C>
struct has_proposal {
    using type = No;
};

/**
 * @brief State that is used by inference algorithms. 
 * 
 * Some inference algorithms are stateless (e.g., likelihood weighting), but others
 * may depend on state (e.g., proposal distributions, past samples from the posterior)
 * 
 * By default, this contains no state; inference algorithms that contain state should
 * specialize this struct and refer to it within `.step(...)` and `.operator()(...)`.
 * 
 * @tparam C Inference algorithm template
 * @tparam I Input type of the probabilistic program
 * @tparam O Output type of the probabilistic program
 * @tparam Ts Types of the distributions in the probabilistic program
 */
template<
    template<class, class, class, template<class, class, class...> class, typename...> class C,
    typename I,
    typename O,
    typename... Ts
>
struct inference_state {};

/**
 * @brief An exogenous proposal distribution. 
 * 
 * An exogenous proposal distribution \f$q(z|i)\f$
 * uses the probabilistic program's inputs to
 * construct a proposal to some or all of the latent address space.
 * An exog_proposal_t returns a double that corresponds to the log probability 
 * of the proposal distribution (e.g., `loglatent(r)`).
 * 
 * @tparam I the type of the input to the probabilistic program
 * @tparam Ts the types of distributions in the probabilistic program
 */
template<typename I, typename... Ts>
using exog_proposal_t = pp_t<I, double, Ts...>;

/**
 * @brief An endogenous proposal distribution.
 * 
 * An endogenous proposal distribution \f$q(z|z')\f$ uses a past or alternate
 * representation of the posterior distribution to construct a new representation (e.g.,
 * as is typical in generic Markov Chain Monte Carlo algorithms). An endog_proposal_t
 * returns a double that corresponds to the log probability 
 * of the proposal distribution (e.g., `loglatent(r)`).
 * 
 * @tparam Ts the types of distributions in the probabilistic program
 */
template<typename... Ts>
using endog_proposal_t = pp_t<record_t<DTypes<Ts...>>&, double, Ts...>;

/**
 * @brief Universal base class for inference methods. 
 * 
 * @tparam A The class template of the inference method
 * @tparam I The type of the input data
 * @tparam O The type of the output data
 * @tparam V The type of the query variable(s)
 * @tparam Q The class template of the queryer
 * @tparam HasProposal will the passed class template have an associated proposal
 *  class?
 * @tparam Ts The distribution types in the program
 */
template<
    template<
        typename,
        typename,
        typename,
        template<class, class, class...> class,
        typename...
    > class A,
    typename I,
    typename O,
    typename V,
    template<class, class, class...> class Q,
    typename... Ts
>
struct Inference {
    pp_t<I, O, Ts...> f;
    Q<V, O, Ts...>& queryer;
    inf_options_t opts; 
    inference_state<A, I, O, Ts...> state;

    Inference(
        pp_t<I, O, Ts...> f,
        Q<V, O, Ts...>& queryer,
        inf_options_t opts
    ) : f(f), queryer(queryer), opts(opts) {}

    void step(record_t<DTypes<Ts...>>& r, I& input) {
        static_cast<A<I, O, V, Q, Ts...>*>(this)->step(r, input);
    }

    /**
     * @brief Runs the specified inference algorithm with the specified queryer using
     *  the specified proposal distribution.
     * 
     * This operator exists only for inference algorithms that require a endogenous proposal distribution (e.g.,
     * generic metropolis-hastings)
     * 
     * @param input the input data to the probabilistic program
     * @param proposal the proposal distribution
     * @return V The return value of the queryer
     */
    template<
        template<
            typename,
            typename,
            typename,
            template<class, class, class...> class,
            typename...
        > class G = A
    >
    std::enable_if_t<std::is_same_v<typename has_proposal<G>::type, Endog>, V> operator()(I& input, endog_proposal_t<Ts...>& proposal) {
        return static_cast<A<I, O, V, Q, Ts...>*>(this)->operator()(input, proposal);
    }

    /**
     * @brief Runs the specified inference algorithm with the specified queryer using
     *  the specified proposal distribution.
     * 
     * This operator exists only for inference algorithms that require a exogenous proposal distribution (e.g.,
     * generic importance sampling)
     * 
     * @param input the input data to the probabilistic program
     * @param proposal the proposal distribution
     * @return V The return value of the queryer
     */
    template<
        template<
            typename,
            typename,
            typename,
            template<class, class, class...> class,
            typename...
        > class G = A
    >
    std::enable_if_t<std::is_same_v<typename has_proposal<G>::type, Exog>, V> operator()(I& input, exog_proposal_t<I, Ts...>& proposal) {
        return static_cast<A<I, O, V, Q, Ts...>*>(this)->operator()(input, proposal);
    }

    /**
     * @brief Runs the specified inference algorithm with the specified queryer.
     * 
     * This operator exists only for inference algorithms that do not require a proposal distribution.
     * 
     * @param input the input data to the probabilistic program
     * @return V The return value of the queryer
     */
    template<
        template<
            typename,
            typename,
            typename,
            template<class, class, class...> class,
            typename...
        > class G = A
    >
    std::enable_if_t<std::is_same_v<typename has_proposal<G>::type, No>, V> operator()(I& input) {
        return static_cast<A<I, O, V, Q, Ts...>*>(this)->operator()(input);
    }

};

/**
 * @brief Factory function for Inference instances
 * 
 */
template<
    template<
        typename,
        typename,
        typename,
        template<class, class, class...> class Q,
        typename...
    > class A,
    typename I,
    typename O,
    typename V,
    template<class, class, class...> class Q,
    typename... Ts
>
Inference<A, I, O, V, Q, Ts...>
inference(pp_t<I, O, Ts...> f, Q<V, O, Ts...>& queryer, inf_options_t opts) {
    return Inference<A, I, O, V, Q, Ts...>(f, queryer, opts);
}

#endif  // INFERENCE_H
