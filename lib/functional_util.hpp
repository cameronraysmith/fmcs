/* 
 * This file is part of fmcs.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Some rights reserved.
 */


#ifndef FUNCTIONAL_UTIL_H
#define FUNCTIONAL_UTIL_H


#include <array>
#include <memory>
#include <string>
#include <tuple>
#include <type_traits>
#include <variant>
#include <vector>


template<typename T, typename... Ts>
struct cat;

template<typename... Ts, typename... Us, typename... Vs>
struct cat<std::variant<Ts...>, std::variant<Us...>, Vs...> 
    : cat<std::variant<Ts..., Us...>, Vs...> {};

template<typename... Ts, typename... Us>
struct cat<std::variant<Ts...>, Us...> {
    using type = std::variant<Ts..., Us...>;
};

std::vector<double> _to_uniform_vector(int dim) {
    std::vector<double> probs;
    for (int i = 0; i != dim; i++) {
        probs.push_back(1.0 / dim);
    }
    return probs;
}


template<typename... Ts>
struct overloaded : Ts... {
    using Ts::operator()...;
};
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

template<typename T, typename...>
std::string stringify(T t) {
    return std::to_string(t);
}

template<typename T>
std::string stringify(std::vector<T> t) {
    std::string out = "[ ";
    for (size_t ix = 0; ix != t.size(); ix++) {
        out += stringify(t[ix]) + " ";
    }
    out += "]";
    return out;
}

template<typename T, size_t N>
std::string stringify(std::array<T, N> t) {
    std::string out = "[ ";
    for (size_t ix = 0; ix != N; ix++) {
        out += stringify(t[ix]) + " ";
    }
    out += "]";
    return out;
}

template<typename T>
std::string stringify(std::shared_ptr<T> t) {
    return "(shared ptr) " + stringify(*t);
}

// Piotr Skotnicki's implementation

template <typename T, typename... Ts>
struct filter_duplicates { using type = T; };

template <template <typename...> class C, typename... Ts, typename U, typename... Us>
struct filter_duplicates<C<Ts...>, U, Us...>
    : std::conditional_t<(std::is_same_v<U, Ts> || ...)
                       , filter_duplicates<C<Ts...>, Us...>
                       , filter_duplicates<C<Ts..., U>, Us...>> {};

template <typename T>
struct unique_variant;

template <typename... Ts>
struct unique_variant<std::variant<Ts...>> : filter_duplicates<std::variant<>, Ts...> {};

template <typename T>
using unique_variant_t = typename unique_variant<T>::type;

// end Piotr Skotnicki's implementation

template<typename... Ts>
struct Holder {};

template<typename T, typename X1, typename... Xi>
struct same_types_walk;

template<typename T, typename X1>
struct same_types_walk<T, X1> : std::is_same<T, X1> {};

template<typename T, typename X1, typename... Xi>
struct same_types_walk
    : std::conditional_t<
        std::is_same<T, X1>::value,
        std::true_type,
        same_types_walk<T, Xi...>
    > {};

template<class L, class R>
struct type_subset;

template<typename L, typename... Rs>
struct type_subset<Holder<L>, Holder<Rs...>> : same_types_walk<L, Rs...> {};

template<typename L, typename... Ls, typename... Rs>
struct type_subset<Holder<L, Ls...>, Holder<Rs...>> 
    : std::conditional_t<
        same_types_walk<L, Rs...>::value,
        type_subset<Holder<Ls...>, Holder<Rs...>>,
        std::false_type
    >{};
    
template<typename L, typename R>
struct type_subset_either;

template<typename... Ls, typename... Rs>
struct type_subset_either<Holder<Ls...>, Holder<Rs...>>
    : std::disjunction<
        type_subset<Holder<Ls...>, Holder<Rs...>>, 
        type_subset<Holder<Rs...>, Holder<Ls...>>
    > {};

template<typename L, typename R>
struct same_size;

template<typename L, typename... Ls, typename R, typename... Rs>
struct same_size<Holder<L, Ls...>, Holder<R, Rs...>> 
    : same_size<Holder<Ls...>, Holder<Rs...>> {};
    
template<typename L, typename... Ls>
struct same_size<Holder<L, Ls...>, Holder<>> : std::false_type {};

template<typename R, typename... Rs>
struct same_size<Holder<>, Holder<R, Rs...>> : std::false_type {};

template<typename L, typename R>
struct same_size<Holder<L>, Holder<R>> : std::true_type {};

template<typename L, typename R>
struct same_types;

template<typename... Ls, typename... Rs>
struct same_types<Holder<Ls...>, Holder<Rs...>>
    : std::conjunction<
        type_subset_either<Holder<Ls...>, Holder<Rs...>>,
        same_size<Holder<Ls...>, Holder<Rs...>>
    > {};
    
template<typename From, typename To>
struct variant_supertype;

template<typename... From, typename... To>
struct variant_supertype<Holder<From...>, Holder<To...>> {

    static_assert( type_subset<Holder<From...>, Holder<To...>>::value );
    
    static std::variant<To...> convert(std::variant<From...>&& f) {
        return std::visit(
            [](auto&& value) -> std::variant<To...> { return std::forward<decltype(value)>(value); },
            std::forward<std::variant<From...>>(f)
        );   
    }
    
};

#endif  // FUNCTIONAL_UTIL_H